#include <iostream>
#include <sstream>
#include <iomanip>
#include <cmath>
#include <cstdlib>
#include <exception>
#include "APFEL/APFEL.h"
using namespace std;


inline string getEnv(const string& name, const string& fallback="ERR") {
  const char* env = std::getenv(name.c_str());
  return env ? string(env) : fallback;
}

template <typename T>
inline T getEnv(const string& name) {
  const string senv = getEnv(name);
  T var;
  istringstream iss;
  iss.str(senv);
  iss >> var;
  if (iss.fail()) throw runtime_error("Invalid env variable");
  return var;
}

template <typename T>
inline T getEnv(const string& name, const T& fallback) {
  const string senv = getEnv(name);
  T var;
  try {
    istringstream iss;
    iss.str(senv);
    iss >> var;
    if (iss.fail()) return fallback;
  } catch (...) {
    return fallback;
  }
  return var;
}


extern "C" void externalsetapfel_(const double& x, const double& /* Q */, double* xf) {
  const double Apl = getEnv<double>("PDF_APL");
  const double Bpl = getEnv<double>("PDF_BPL");
  const double Ahad = getEnv<double>("PDF_AHAD");
  const double Bhad = getEnv<double>("PDF_BHAD");
  const double Chad = getEnv<double>("PDF_CHAD", 1.0);
  const double Ahadg = getEnv<double>("PDF_AHADG");
  const double Bhadg = getEnv<double>("PDF_BHADG");
  const double Chadg = getEnv<double>("PDF_CHADG", 3.0);

  const double xbar = 1 - x;
  const double f_pl_e2 = Apl * (x*x + xbar*xbar) / (1 - Bpl*log(xbar));
  const double f_had_ud = Ahad * pow(x, Bhad) * pow(xbar, Chad);
  const double f_had_s = 0.3 * f_had_ud;
  const double f_had_g = Ahadg * pow(x, Bhadg) * pow(xbar, Chadg);

  xf[0] = 0.0; // tbar
  xf[1] = 0.0; // bbar
  xf[2] = 0.0; // cbar
  xf[3] = x * (1/9. * f_pl_e2 + f_had_s); // sbar
  xf[4] = x * (4/9. * f_pl_e2 + f_had_ud); // ubar
  xf[5] = x * (1/9. * f_pl_e2 + f_had_ud); // dbar
  xf[6] = x * f_had_g; // gluon
  xf[7] = xf[5]; // d
  xf[8] = xf[4]; // u
  xf[9] = xf[3]; // s
  xf[10] = 0.0; // c
  xf[11] = 0.0; // b
  xf[12] = 0.0; // t
  xf[13] = 0.0; // gamma
}


int main() {

  // Grid points
  const double Qs[] = {1.0, 5.0, 10.0, 50.0, 100.0, 1000.0, 10000.0};
  const double xs[] = {1e-5, 1e-4, 1e-3, 1e-2, 1e-1, 3e-1, 5e-1, 7e-1, 9e-1};

  // Activate some options
  APFEL::SetPerturbativeOrder(2);
  APFEL::SetPDFSet("external");

  // Initialize integrals on the grids
  APFEL::InitializeAPFEL();

  // Print headings
  cout << "   x   "
       << setw(11) << "   u-ubar    "
       << setw(11) << "   d-dbar    "
       << setw(11) << "  2(ubr+dbr) "
       << setw(11) << "   c+cbar    "
       << setw(11) << "   gluon     "
       << setw(11) << "   photon    " << endl;

  // Load evolution
  const double Q0 = 1.0 - 1e-10;
  for (const double Q : Qs) {
    APFEL::EvolveAPFEL(Q0, Q);

    cout << scientific << setprecision(5) << endl;
    // Tabulate PDFs for the LHA x values
    cout << "alpha_QCD(mu2F) = " << APFEL::AlphaQCD(Q) << endl;
    cout << "alpha_QED(mu2F) = " << APFEL::AlphaQED(Q) << endl;
    cout << scientific;
    for (double x : xs) {
      cout << setprecision(1)
           << x << "\t" << setprecision(4)
           << setw(11) <<  APFEL::xPDF(2,x) - APFEL::xPDF(-2,x) << "  "
           << setw(11) <<  APFEL::xPDF(1,x) - APFEL::xPDF(-1,x) << "  "
           << setw(11) << 2*(APFEL::xPDF(-1,x) + APFEL::xPDF(-2,x)) << "  "
           << setw(11) <<  APFEL::xPDF(4,x) + APFEL::xPDF(-4,x) << "  "
           << setw(11) <<  APFEL::xPDF(0,x) << "  "
           << setw(11) <<  APFEL::xgamma(x) << "  "
           << endl;
    }
    for (const double x : xs) {
      cout << "x: " << x << endl;
      for (const double Q : Qs) {
        APFEL::EvolveAPFEL(Q0, Q);
        cout << setprecision(1)
             << x << "\t" << setprecision(4)
             << setw(11) <<  APFEL::xPDF(2,x) - APFEL::xPDF(-2,x) << "  "
            << setw(11) <<  APFEL::xPDF(1,x) - APFEL::xPDF(-1,x) << "  "
            << setw(11) << 2*(APFEL::xPDF(-1,x) + APFEL::xPDF(-2,x)) << "  "
            << setw(11) <<  APFEL::xPDF(4,x) + APFEL::xPDF(-4,x) << "  "
            << endl;
      }
    }


    // Blank line
    cout << endl;
  }

  return 0;
}
