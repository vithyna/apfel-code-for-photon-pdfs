# ProfVersion: 2.4.2
# Date: 2024-03-11 22:57:17
# InterpolationFile: /home/narendran/Documents/CEDAR_stuff2/apfel-code-for-photon-pdfs/RUN2/ipol.dat
# DataDirectory: /home/narendran/Documents/CEDAR_stuff2/apfel-code-for-photon-pdfs/RUN2/prof_refdata_98
#
# Limits:
#	PDF_APL  	0.000965 0.998800
#	PDF_BPL  	-0.977988 1.974870
#	PDF_AHAD 	0.001407 0.999209
#	PDF_BHAD 	-0.986509 1.998866
#	PDF_AHADG	0.004202 1.996735
#	PDF_BHADG	-0.989979 1.485580
#
# Fixed:
#
# Minimisation result:
#
# GOF 1246.993078
# UNITGOF 1246.993078
# NDOF 237.000000
PDF_APL  	0.267491
PDF_BPL  	-0.342007
PDF_AHAD 	0.204791
PDF_BHAD 	0.090987
PDF_AHADG	1.911492
PDF_BHADG	0.742360
#
# MIGRAD errors:
#
# PDF_APL  	1.608487e-03
# PDF_BPL  	1.788352e-03
# PDF_AHAD 	1.248939e-03
# PDF_BHAD 	1.634350e-03
# PDF_AHADG	6.800626e-04
# PDF_BHADG	1.434595e-03
#
# Correlation matrix:
#
#
#
# Weights used
#
# /H1_2002_I581409/d01-x01-y01      0.6   # 6 bins from 45 to 180
# /H1_2002_I581409/d02-x01-y01      0.6   # 4 bins from 20 to 80
# /H1_2002_I581409/d03-x01-y01      0.6   # 4 bins from 25 to 80
# /H1_2002_I581409/d04-x01-y01      0.6   # 5 bins from 0.6 to 2.5
# /H1_2002_I581409/d04-x01-y02      0.6   # 4 bins from 0.9 to 2.5
# /H1_2002_I581409/d05-x01-y01      0.6   # 6 bins from 0 to 2.5
# /H1_2002_I581409/d05-x01-y02      0.6   # 4 bins from 0.6 to 2.1
# /H1_2002_I581409/d06-x01-y01      0.8   # 3 bins from 0.5 to 1
# /H1_2002_I581409/d06-x01-y02      0.8   # 5 bins from 0.1 to 1
# /H1_2002_I581409/d07-x01-y01      1.0   # 5 bins from 0.05 to 0.6
# /H1_2002_I581409/d07-x01-y02      0.1   # 5 bins from 0.05 to 0.6
# /H1_2002_I581409/d08-x01-y01      0.8   # 5 bins from 0.1 to 1
# /H1_2002_I581409/d08-x01-y02      0.8   # 5 bins from 0.1 to 1
# /H1_2002_I581409/d09-x01-y01      1.0   # 7 bins from 0 to 0.7
# /H1_2002_I581409/d09-x01-y02      0.1   # 7 bins from 0 to 0.7
# /H1_2002_I581409/d10-x01-y01      1.0   # 8 bins from 0 to 0.85
# /H1_2002_I581409/d10-x01-y02      0.1   # 8 bins from 0 to 0.85
# /ZEUS_1997_I450085/d01-x01-y01    0.5   # 11 bins from -1.125 to 1.625
# /ZEUS_1997_I450085/d02-x01-y01    0.5   # 10 bins from -0.875 to 1.625
# /ZEUS_1997_I450085/d03-x01-y01    0.5   # 4 bins from -0.375 to 1.625
# /ZEUS_1997_I450085/d04-x01-y01    0.5   # 4 bins from -0.375 to 1.625
# /ZEUS_1997_I450085/d21-x01-y01    0.1   # 9 bins from -1.125 to 1.125
# /ZEUS_1997_I450085/d22-x01-y01    0.1   # 8 bins from -0.875 to 1.125
# /ZEUS_1997_I450085/d23-x01-y01    0.1   # 4 bins from -0.375 to 1.625
# /ZEUS_1997_I450085/d24-x01-y01    0.1   # 4 bins from -0.375 to 1.625
# /ZEUS_1997_I450085/d25-x01-y01    1.0   # 8 bins from -0.375 to 1.5
# /ZEUS_1997_I450085/d26-x01-y01    1.0   # 8 bins from -0.375 to 1.625
# /ZEUS_1997_I450085/d27-x01-y01    1.0   # 3 bins from 0.125 to 1.625
# /ZEUS_1997_I450085/d28-x01-y01    1.0   # 3 bins from 0.125 to 1.625
# /ZEUS_2012_I1116258/d01-x01-y01   0.6   # 9 bins from 17 to 95
# /ZEUS_2012_I1116258/d02-x01-y01   0.6   # 13 bins from -0.75 to 2.5
# /ZEUS_2012_I1116258/d03-x01-y01   0.6   # 13 bins from -0.75 to 2.5
# /ZEUS_2012_I1116258/d04-x01-y01   0.6   # 5 bins from 17 to 41
# /ZEUS_2012_I1116258/d05-x01-y01   0.6   # 9 bins from 17 to 95
# /ZEUS_2012_I1116258/d06-x01-y01   0.6   # 9 bins from 17 to 95
# /ZEUS_2012_I1116258/d07-x01-y01   0.6   # 9 bins from 17 to 95
# /ZEUS_2012_I1116258/d08-x01-y01   0.6   # 9 bins from 17 to 95
# /ZEUS_2012_I1116258/d09-x01-y01   0.0   # 9 bins from 17 to 95
# /ZEUS_2012_I1116258/d10-x01-y01   0.0   # 9 bins from 17 to 95
# /ZEUS_2012_I1116258/d11-x01-y01   0.0   # 13 bins from -0.75 to 2.5
# /ZEUS_2012_I1116258/d12-x01-y01   0.0   # 13 bins from -0.75 to 2.5
#
# Mean contribution:
#
# /H1_2002_I581409/d01-x01-y01	1.325775
# /H1_2002_I581409/d02-x01-y01	1.389436
# /H1_2002_I581409/d03-x01-y01	1.839459
# /H1_2002_I581409/d04-x01-y01	1.718349
# /H1_2002_I581409/d04-x01-y02	1.604048
# /H1_2002_I581409/d05-x01-y01	3.382069
# /H1_2002_I581409/d05-x01-y02	1.460328
# /H1_2002_I581409/d06-x01-y01	2.493261
# /H1_2002_I581409/d06-x01-y02	11.023911
# /H1_2002_I581409/d07-x01-y01	1.729793
# /H1_2002_I581409/d07-x01-y02	0.064892
# /H1_2002_I581409/d08-x01-y01	8.868357
# /H1_2002_I581409/d08-x01-y02	4.810452
# /H1_2002_I581409/d09-x01-y01	1.814107
# /H1_2002_I581409/d09-x01-y02	0.057220
# /H1_2002_I581409/d10-x01-y01	2.106508
# /H1_2002_I581409/d10-x01-y02	0.040785
# /ZEUS_1997_I450085/d01-x01-y01	3.945288
# /ZEUS_1997_I450085/d02-x01-y01	1.877462
# /ZEUS_1997_I450085/d03-x01-y01	2.400929
# /ZEUS_1997_I450085/d04-x01-y01	0.495433
# /ZEUS_1997_I450085/d21-x01-y01	0.111408
# /ZEUS_1997_I450085/d22-x01-y01	0.049021
# /ZEUS_1997_I450085/d23-x01-y01	0.008736
# /ZEUS_1997_I450085/d24-x01-y01	0.011217
# /ZEUS_1997_I450085/d25-x01-y01	27.742270
# /ZEUS_1997_I450085/d26-x01-y01	20.893894
# /ZEUS_1997_I450085/d27-x01-y01	21.990425
# /ZEUS_1997_I450085/d28-x01-y01	2.840675
# /ZEUS_2012_I1116258/d01-x01-y01	2.996758
# /ZEUS_2012_I1116258/d02-x01-y01	14.219072
# /ZEUS_2012_I1116258/d03-x01-y01	5.433761
# /ZEUS_2012_I1116258/d04-x01-y01	7.515800
# /ZEUS_2012_I1116258/d05-x01-y01	7.533003
# /ZEUS_2012_I1116258/d06-x01-y01	5.798338
# /ZEUS_2012_I1116258/d07-x01-y01	1.640341
# /ZEUS_2012_I1116258/d08-x01-y01	2.348827
