# ProfVersion: 2.4.2
# Date: 2024-03-11 22:57:22
# InterpolationFile: /home/narendran/Documents/CEDAR_stuff2/apfel-code-for-photon-pdfs/RUN2/ipol.dat
# DataDirectory: /home/narendran/Documents/CEDAR_stuff2/apfel-code-for-photon-pdfs/RUN2/prof_refdata_86
#
# Limits:
#	PDF_APL  	0.000965 0.998800
#	PDF_BPL  	-0.977988 1.974870
#	PDF_AHAD 	0.001407 0.999209
#	PDF_BHAD 	-0.986509 1.998866
#	PDF_AHADG	0.004202 1.996735
#	PDF_BHADG	-0.989979 1.485580
#
# Fixed:
#
# Minimisation result:
#
# GOF 1263.420848
# UNITGOF 1263.420848
# NDOF 237.000000
PDF_APL  	0.267112
PDF_BPL  	-0.346438
PDF_AHAD 	0.204693
PDF_BHAD 	0.098669
PDF_AHADG	1.914755
PDF_BHADG	0.749525
#
# MIGRAD errors:
#
# PDF_APL  	3.006641e-03
# PDF_BPL  	3.242843e-03
# PDF_AHAD 	2.324824e-03
# PDF_BHAD 	2.622934e-03
# PDF_AHADG	5.378222e-04
# PDF_BHADG	1.259334e-03
#
# Correlation matrix:
#
#
#
# Weights used
#
# /H1_2002_I581409/d01-x01-y01      0.6   # 6 bins from 45 to 180
# /H1_2002_I581409/d02-x01-y01      0.6   # 4 bins from 20 to 80
# /H1_2002_I581409/d03-x01-y01      0.6   # 4 bins from 25 to 80
# /H1_2002_I581409/d04-x01-y01      0.6   # 5 bins from 0.6 to 2.5
# /H1_2002_I581409/d04-x01-y02      0.6   # 4 bins from 0.9 to 2.5
# /H1_2002_I581409/d05-x01-y01      0.6   # 6 bins from 0 to 2.5
# /H1_2002_I581409/d05-x01-y02      0.6   # 4 bins from 0.6 to 2.1
# /H1_2002_I581409/d06-x01-y01      0.8   # 3 bins from 0.5 to 1
# /H1_2002_I581409/d06-x01-y02      0.8   # 5 bins from 0.1 to 1
# /H1_2002_I581409/d07-x01-y01      1.0   # 5 bins from 0.05 to 0.6
# /H1_2002_I581409/d07-x01-y02      0.1   # 5 bins from 0.05 to 0.6
# /H1_2002_I581409/d08-x01-y01      0.8   # 5 bins from 0.1 to 1
# /H1_2002_I581409/d08-x01-y02      0.8   # 5 bins from 0.1 to 1
# /H1_2002_I581409/d09-x01-y01      1.0   # 7 bins from 0 to 0.7
# /H1_2002_I581409/d09-x01-y02      0.1   # 7 bins from 0 to 0.7
# /H1_2002_I581409/d10-x01-y01      1.0   # 8 bins from 0 to 0.85
# /H1_2002_I581409/d10-x01-y02      0.1   # 8 bins from 0 to 0.85
# /ZEUS_1997_I450085/d01-x01-y01    0.5   # 11 bins from -1.125 to 1.625
# /ZEUS_1997_I450085/d02-x01-y01    0.5   # 10 bins from -0.875 to 1.625
# /ZEUS_1997_I450085/d03-x01-y01    0.5   # 4 bins from -0.375 to 1.625
# /ZEUS_1997_I450085/d04-x01-y01    0.5   # 4 bins from -0.375 to 1.625
# /ZEUS_1997_I450085/d21-x01-y01    0.1   # 9 bins from -1.125 to 1.125
# /ZEUS_1997_I450085/d22-x01-y01    0.1   # 8 bins from -0.875 to 1.125
# /ZEUS_1997_I450085/d23-x01-y01    0.1   # 4 bins from -0.375 to 1.625
# /ZEUS_1997_I450085/d24-x01-y01    0.1   # 4 bins from -0.375 to 1.625
# /ZEUS_1997_I450085/d25-x01-y01    1.0   # 8 bins from -0.375 to 1.5
# /ZEUS_1997_I450085/d26-x01-y01    1.0   # 8 bins from -0.375 to 1.625
# /ZEUS_1997_I450085/d27-x01-y01    1.0   # 3 bins from 0.125 to 1.625
# /ZEUS_1997_I450085/d28-x01-y01    1.0   # 3 bins from 0.125 to 1.625
# /ZEUS_2012_I1116258/d01-x01-y01   0.6   # 9 bins from 17 to 95
# /ZEUS_2012_I1116258/d02-x01-y01   0.6   # 13 bins from -0.75 to 2.5
# /ZEUS_2012_I1116258/d03-x01-y01   0.6   # 13 bins from -0.75 to 2.5
# /ZEUS_2012_I1116258/d04-x01-y01   0.6   # 5 bins from 17 to 41
# /ZEUS_2012_I1116258/d05-x01-y01   0.6   # 9 bins from 17 to 95
# /ZEUS_2012_I1116258/d06-x01-y01   0.6   # 9 bins from 17 to 95
# /ZEUS_2012_I1116258/d07-x01-y01   0.6   # 9 bins from 17 to 95
# /ZEUS_2012_I1116258/d08-x01-y01   0.6   # 9 bins from 17 to 95
# /ZEUS_2012_I1116258/d09-x01-y01   0.0   # 9 bins from 17 to 95
# /ZEUS_2012_I1116258/d10-x01-y01   0.0   # 9 bins from 17 to 95
# /ZEUS_2012_I1116258/d11-x01-y01   0.0   # 13 bins from -0.75 to 2.5
# /ZEUS_2012_I1116258/d12-x01-y01   0.0   # 13 bins from -0.75 to 2.5
#
# Mean contribution:
#
# /H1_2002_I581409/d01-x01-y01	0.926733
# /H1_2002_I581409/d02-x01-y01	1.489791
# /H1_2002_I581409/d03-x01-y01	0.887369
# /H1_2002_I581409/d04-x01-y01	1.951600
# /H1_2002_I581409/d04-x01-y02	0.739705
# /H1_2002_I581409/d05-x01-y01	4.348332
# /H1_2002_I581409/d05-x01-y02	1.251879
# /H1_2002_I581409/d06-x01-y01	3.026620
# /H1_2002_I581409/d06-x01-y02	9.129300
# /H1_2002_I581409/d07-x01-y01	0.762947
# /H1_2002_I581409/d07-x01-y02	0.072627
# /H1_2002_I581409/d08-x01-y01	5.007814
# /H1_2002_I581409/d08-x01-y02	4.467035
# /H1_2002_I581409/d09-x01-y01	1.699438
# /H1_2002_I581409/d09-x01-y02	0.073185
# /H1_2002_I581409/d10-x01-y01	2.237738
# /H1_2002_I581409/d10-x01-y02	0.039962
# /ZEUS_1997_I450085/d01-x01-y01	4.471835
# /ZEUS_1997_I450085/d02-x01-y01	1.772458
# /ZEUS_1997_I450085/d03-x01-y01	2.615886
# /ZEUS_1997_I450085/d04-x01-y01	0.596753
# /ZEUS_1997_I450085/d21-x01-y01	0.123544
# /ZEUS_1997_I450085/d22-x01-y01	0.041107
# /ZEUS_1997_I450085/d23-x01-y01	0.012993
# /ZEUS_1997_I450085/d24-x01-y01	0.024465
# /ZEUS_1997_I450085/d25-x01-y01	29.033508
# /ZEUS_1997_I450085/d26-x01-y01	20.838779
# /ZEUS_1997_I450085/d27-x01-y01	21.197950
# /ZEUS_1997_I450085/d28-x01-y01	1.485047
# /ZEUS_2012_I1116258/d01-x01-y01	5.098085
# /ZEUS_2012_I1116258/d02-x01-y01	13.205953
# /ZEUS_2012_I1116258/d03-x01-y01	6.400339
# /ZEUS_2012_I1116258/d04-x01-y01	10.381930
# /ZEUS_2012_I1116258/d05-x01-y01	7.007481
# /ZEUS_2012_I1116258/d06-x01-y01	7.156676
# /ZEUS_2012_I1116258/d07-x01-y01	1.953451
# /ZEUS_2012_I1116258/d08-x01-y01	2.390353
