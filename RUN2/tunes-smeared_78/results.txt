# ProfVersion: 2.4.2
# Date: 2024-03-11 22:57:14
# InterpolationFile: /home/narendran/Documents/CEDAR_stuff2/apfel-code-for-photon-pdfs/RUN2/ipol.dat
# DataDirectory: /home/narendran/Documents/CEDAR_stuff2/apfel-code-for-photon-pdfs/RUN2/prof_refdata_78
#
# Limits:
#	PDF_APL  	0.000965 0.998800
#	PDF_BPL  	-0.977988 1.974870
#	PDF_AHAD 	0.001407 0.999209
#	PDF_BHAD 	-0.986509 1.998866
#	PDF_AHADG	0.004202 1.996735
#	PDF_BHADG	-0.989979 1.485580
#
# Fixed:
#
# Minimisation result:
#
# GOF 1283.927427
# UNITGOF 1283.927427
# NDOF 237.000000
PDF_APL  	0.285006
PDF_BPL  	-0.368242
PDF_AHAD 	0.217311
PDF_BHAD 	0.076001
PDF_AHADG	1.919028
PDF_BHADG	0.760978
#
# MIGRAD errors:
#
# PDF_APL  	7.627543e-04
# PDF_BPL  	1.047395e-03
# PDF_AHAD 	5.638330e-04
# PDF_BHAD 	1.169378e-03
# PDF_AHADG	6.070280e-04
# PDF_BHADG	1.444698e-03
#
# Correlation matrix:
#
#
#
# Weights used
#
# /H1_2002_I581409/d01-x01-y01      0.6   # 6 bins from 45 to 180
# /H1_2002_I581409/d02-x01-y01      0.6   # 4 bins from 20 to 80
# /H1_2002_I581409/d03-x01-y01      0.6   # 4 bins from 25 to 80
# /H1_2002_I581409/d04-x01-y01      0.6   # 5 bins from 0.6 to 2.5
# /H1_2002_I581409/d04-x01-y02      0.6   # 4 bins from 0.9 to 2.5
# /H1_2002_I581409/d05-x01-y01      0.6   # 6 bins from 0 to 2.5
# /H1_2002_I581409/d05-x01-y02      0.6   # 4 bins from 0.6 to 2.1
# /H1_2002_I581409/d06-x01-y01      0.8   # 3 bins from 0.5 to 1
# /H1_2002_I581409/d06-x01-y02      0.8   # 5 bins from 0.1 to 1
# /H1_2002_I581409/d07-x01-y01      1.0   # 5 bins from 0.05 to 0.6
# /H1_2002_I581409/d07-x01-y02      0.1   # 5 bins from 0.05 to 0.6
# /H1_2002_I581409/d08-x01-y01      0.8   # 5 bins from 0.1 to 1
# /H1_2002_I581409/d08-x01-y02      0.8   # 5 bins from 0.1 to 1
# /H1_2002_I581409/d09-x01-y01      1.0   # 7 bins from 0 to 0.7
# /H1_2002_I581409/d09-x01-y02      0.1   # 7 bins from 0 to 0.7
# /H1_2002_I581409/d10-x01-y01      1.0   # 8 bins from 0 to 0.85
# /H1_2002_I581409/d10-x01-y02      0.1   # 8 bins from 0 to 0.85
# /ZEUS_1997_I450085/d01-x01-y01    0.5   # 11 bins from -1.125 to 1.625
# /ZEUS_1997_I450085/d02-x01-y01    0.5   # 10 bins from -0.875 to 1.625
# /ZEUS_1997_I450085/d03-x01-y01    0.5   # 4 bins from -0.375 to 1.625
# /ZEUS_1997_I450085/d04-x01-y01    0.5   # 4 bins from -0.375 to 1.625
# /ZEUS_1997_I450085/d21-x01-y01    0.1   # 9 bins from -1.125 to 1.125
# /ZEUS_1997_I450085/d22-x01-y01    0.1   # 8 bins from -0.875 to 1.125
# /ZEUS_1997_I450085/d23-x01-y01    0.1   # 4 bins from -0.375 to 1.625
# /ZEUS_1997_I450085/d24-x01-y01    0.1   # 4 bins from -0.375 to 1.625
# /ZEUS_1997_I450085/d25-x01-y01    1.0   # 8 bins from -0.375 to 1.5
# /ZEUS_1997_I450085/d26-x01-y01    1.0   # 8 bins from -0.375 to 1.625
# /ZEUS_1997_I450085/d27-x01-y01    1.0   # 3 bins from 0.125 to 1.625
# /ZEUS_1997_I450085/d28-x01-y01    1.0   # 3 bins from 0.125 to 1.625
# /ZEUS_2012_I1116258/d01-x01-y01   0.6   # 9 bins from 17 to 95
# /ZEUS_2012_I1116258/d02-x01-y01   0.6   # 13 bins from -0.75 to 2.5
# /ZEUS_2012_I1116258/d03-x01-y01   0.6   # 13 bins from -0.75 to 2.5
# /ZEUS_2012_I1116258/d04-x01-y01   0.6   # 5 bins from 17 to 41
# /ZEUS_2012_I1116258/d05-x01-y01   0.6   # 9 bins from 17 to 95
# /ZEUS_2012_I1116258/d06-x01-y01   0.6   # 9 bins from 17 to 95
# /ZEUS_2012_I1116258/d07-x01-y01   0.6   # 9 bins from 17 to 95
# /ZEUS_2012_I1116258/d08-x01-y01   0.6   # 9 bins from 17 to 95
# /ZEUS_2012_I1116258/d09-x01-y01   0.0   # 9 bins from 17 to 95
# /ZEUS_2012_I1116258/d10-x01-y01   0.0   # 9 bins from 17 to 95
# /ZEUS_2012_I1116258/d11-x01-y01   0.0   # 13 bins from -0.75 to 2.5
# /ZEUS_2012_I1116258/d12-x01-y01   0.0   # 13 bins from -0.75 to 2.5
#
# Mean contribution:
#
# /H1_2002_I581409/d01-x01-y01	1.918946
# /H1_2002_I581409/d02-x01-y01	0.543562
# /H1_2002_I581409/d03-x01-y01	0.515986
# /H1_2002_I581409/d04-x01-y01	2.020604
# /H1_2002_I581409/d04-x01-y02	1.325241
# /H1_2002_I581409/d05-x01-y01	4.174313
# /H1_2002_I581409/d05-x01-y02	1.932435
# /H1_2002_I581409/d06-x01-y01	4.950861
# /H1_2002_I581409/d06-x01-y02	10.006343
# /H1_2002_I581409/d07-x01-y01	2.949821
# /H1_2002_I581409/d07-x01-y02	0.109517
# /H1_2002_I581409/d08-x01-y01	8.206191
# /H1_2002_I581409/d08-x01-y02	4.611625
# /H1_2002_I581409/d09-x01-y01	0.736853
# /H1_2002_I581409/d09-x01-y02	0.042090
# /H1_2002_I581409/d10-x01-y01	2.237429
# /H1_2002_I581409/d10-x01-y02	0.058233
# /ZEUS_1997_I450085/d01-x01-y01	4.312403
# /ZEUS_1997_I450085/d02-x01-y01	2.187767
# /ZEUS_1997_I450085/d03-x01-y01	2.829854
# /ZEUS_1997_I450085/d04-x01-y01	0.496502
# /ZEUS_1997_I450085/d21-x01-y01	0.105981
# /ZEUS_1997_I450085/d22-x01-y01	0.020981
# /ZEUS_1997_I450085/d23-x01-y01	0.017982
# /ZEUS_1997_I450085/d24-x01-y01	0.009013
# /ZEUS_1997_I450085/d25-x01-y01	28.999275
# /ZEUS_1997_I450085/d26-x01-y01	23.349090
# /ZEUS_1997_I450085/d27-x01-y01	22.945139
# /ZEUS_1997_I450085/d28-x01-y01	0.676909
# /ZEUS_2012_I1116258/d01-x01-y01	3.199663
# /ZEUS_2012_I1116258/d02-x01-y01	12.688192
# /ZEUS_2012_I1116258/d03-x01-y01	6.008751
# /ZEUS_2012_I1116258/d04-x01-y01	9.580214
# /ZEUS_2012_I1116258/d05-x01-y01	7.785460
# /ZEUS_2012_I1116258/d06-x01-y01	5.805501
# /ZEUS_2012_I1116258/d07-x01-y01	2.000089
# /ZEUS_2012_I1116258/d08-x01-y01	2.036444
