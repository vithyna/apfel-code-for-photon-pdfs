# ProfVersion: 2.4.2
# Date: 2024-03-11 22:57:20
# InterpolationFile: /home/narendran/Documents/CEDAR_stuff2/apfel-code-for-photon-pdfs/RUN2/ipol.dat
# DataDirectory: /home/narendran/Documents/CEDAR_stuff2/apfel-code-for-photon-pdfs/RUN2/prof_refdata_92
#
# Limits:
#	PDF_APL  	0.000965 0.998800
#	PDF_BPL  	-0.977988 1.974870
#	PDF_AHAD 	0.001407 0.999209
#	PDF_BHAD 	-0.986509 1.998866
#	PDF_AHADG	0.004202 1.996735
#	PDF_BHADG	-0.989979 1.485580
#
# Fixed:
#
# Minimisation result:
#
# GOF 1330.159236
# UNITGOF 1330.159236
# NDOF 237.000000
PDF_APL  	0.282688
PDF_BPL  	-0.371859
PDF_AHAD 	0.216288
PDF_BHAD 	0.084615
PDF_AHADG	1.921302
PDF_BHADG	0.764354
#
# MIGRAD errors:
#
# PDF_APL  	2.881595e-03
# PDF_BPL  	2.129846e-03
# PDF_AHAD 	2.342862e-03
# PDF_BHAD 	2.485298e-03
# PDF_AHADG	1.906055e-04
# PDF_BHADG	4.713532e-04
#
# Correlation matrix:
#
#
#
# Weights used
#
# /H1_2002_I581409/d01-x01-y01      0.6   # 6 bins from 45 to 180
# /H1_2002_I581409/d02-x01-y01      0.6   # 4 bins from 20 to 80
# /H1_2002_I581409/d03-x01-y01      0.6   # 4 bins from 25 to 80
# /H1_2002_I581409/d04-x01-y01      0.6   # 5 bins from 0.6 to 2.5
# /H1_2002_I581409/d04-x01-y02      0.6   # 4 bins from 0.9 to 2.5
# /H1_2002_I581409/d05-x01-y01      0.6   # 6 bins from 0 to 2.5
# /H1_2002_I581409/d05-x01-y02      0.6   # 4 bins from 0.6 to 2.1
# /H1_2002_I581409/d06-x01-y01      0.8   # 3 bins from 0.5 to 1
# /H1_2002_I581409/d06-x01-y02      0.8   # 5 bins from 0.1 to 1
# /H1_2002_I581409/d07-x01-y01      1.0   # 5 bins from 0.05 to 0.6
# /H1_2002_I581409/d07-x01-y02      0.1   # 5 bins from 0.05 to 0.6
# /H1_2002_I581409/d08-x01-y01      0.8   # 5 bins from 0.1 to 1
# /H1_2002_I581409/d08-x01-y02      0.8   # 5 bins from 0.1 to 1
# /H1_2002_I581409/d09-x01-y01      1.0   # 7 bins from 0 to 0.7
# /H1_2002_I581409/d09-x01-y02      0.1   # 7 bins from 0 to 0.7
# /H1_2002_I581409/d10-x01-y01      1.0   # 8 bins from 0 to 0.85
# /H1_2002_I581409/d10-x01-y02      0.1   # 8 bins from 0 to 0.85
# /ZEUS_1997_I450085/d01-x01-y01    0.5   # 11 bins from -1.125 to 1.625
# /ZEUS_1997_I450085/d02-x01-y01    0.5   # 10 bins from -0.875 to 1.625
# /ZEUS_1997_I450085/d03-x01-y01    0.5   # 4 bins from -0.375 to 1.625
# /ZEUS_1997_I450085/d04-x01-y01    0.5   # 4 bins from -0.375 to 1.625
# /ZEUS_1997_I450085/d21-x01-y01    0.1   # 9 bins from -1.125 to 1.125
# /ZEUS_1997_I450085/d22-x01-y01    0.1   # 8 bins from -0.875 to 1.125
# /ZEUS_1997_I450085/d23-x01-y01    0.1   # 4 bins from -0.375 to 1.625
# /ZEUS_1997_I450085/d24-x01-y01    0.1   # 4 bins from -0.375 to 1.625
# /ZEUS_1997_I450085/d25-x01-y01    1.0   # 8 bins from -0.375 to 1.5
# /ZEUS_1997_I450085/d26-x01-y01    1.0   # 8 bins from -0.375 to 1.625
# /ZEUS_1997_I450085/d27-x01-y01    1.0   # 3 bins from 0.125 to 1.625
# /ZEUS_1997_I450085/d28-x01-y01    1.0   # 3 bins from 0.125 to 1.625
# /ZEUS_2012_I1116258/d01-x01-y01   0.6   # 9 bins from 17 to 95
# /ZEUS_2012_I1116258/d02-x01-y01   0.6   # 13 bins from -0.75 to 2.5
# /ZEUS_2012_I1116258/d03-x01-y01   0.6   # 13 bins from -0.75 to 2.5
# /ZEUS_2012_I1116258/d04-x01-y01   0.6   # 5 bins from 17 to 41
# /ZEUS_2012_I1116258/d05-x01-y01   0.6   # 9 bins from 17 to 95
# /ZEUS_2012_I1116258/d06-x01-y01   0.6   # 9 bins from 17 to 95
# /ZEUS_2012_I1116258/d07-x01-y01   0.6   # 9 bins from 17 to 95
# /ZEUS_2012_I1116258/d08-x01-y01   0.6   # 9 bins from 17 to 95
# /ZEUS_2012_I1116258/d09-x01-y01   0.0   # 9 bins from 17 to 95
# /ZEUS_2012_I1116258/d10-x01-y01   0.0   # 9 bins from 17 to 95
# /ZEUS_2012_I1116258/d11-x01-y01   0.0   # 13 bins from -0.75 to 2.5
# /ZEUS_2012_I1116258/d12-x01-y01   0.0   # 13 bins from -0.75 to 2.5
#
# Mean contribution:
#
# /H1_2002_I581409/d01-x01-y01	1.390470
# /H1_2002_I581409/d02-x01-y01	0.779002
# /H1_2002_I581409/d03-x01-y01	1.258942
# /H1_2002_I581409/d04-x01-y01	1.770557
# /H1_2002_I581409/d04-x01-y02	1.903180
# /H1_2002_I581409/d05-x01-y01	4.659732
# /H1_2002_I581409/d05-x01-y02	0.840972
# /H1_2002_I581409/d06-x01-y01	3.161726
# /H1_2002_I581409/d06-x01-y02	8.868915
# /H1_2002_I581409/d07-x01-y01	2.846319
# /H1_2002_I581409/d07-x01-y02	0.053540
# /H1_2002_I581409/d08-x01-y01	9.765998
# /H1_2002_I581409/d08-x01-y02	3.612435
# /H1_2002_I581409/d09-x01-y01	1.468347
# /H1_2002_I581409/d09-x01-y02	0.076599
# /H1_2002_I581409/d10-x01-y01	2.054575
# /H1_2002_I581409/d10-x01-y02	0.056717
# /ZEUS_1997_I450085/d01-x01-y01	3.568178
# /ZEUS_1997_I450085/d02-x01-y01	2.463061
# /ZEUS_1997_I450085/d03-x01-y01	2.343587
# /ZEUS_1997_I450085/d04-x01-y01	0.410255
# /ZEUS_1997_I450085/d21-x01-y01	0.128731
# /ZEUS_1997_I450085/d22-x01-y01	0.023471
# /ZEUS_1997_I450085/d23-x01-y01	0.021721
# /ZEUS_1997_I450085/d24-x01-y01	0.022351
# /ZEUS_1997_I450085/d25-x01-y01	29.652813
# /ZEUS_1997_I450085/d26-x01-y01	18.447473
# /ZEUS_1997_I450085/d27-x01-y01	18.578714
# /ZEUS_1997_I450085/d28-x01-y01	3.933950
# /ZEUS_2012_I1116258/d01-x01-y01	4.345603
# /ZEUS_2012_I1116258/d02-x01-y01	14.873253
# /ZEUS_2012_I1116258/d03-x01-y01	5.593866
# /ZEUS_2012_I1116258/d04-x01-y01	11.834223
# /ZEUS_2012_I1116258/d05-x01-y01	9.704122
# /ZEUS_2012_I1116258/d06-x01-y01	7.998307
# /ZEUS_2012_I1116258/d07-x01-y01	3.128267
# /ZEUS_2012_I1116258/d08-x01-y01	2.484609
