# ProfVersion: 2.4.2
# Date: 2024-03-11 22:51:44
# InterpolationFile: /home/narendran/Documents/CEDAR_stuff2/apfel-code-for-photon-pdfs/RUN2/ipol.dat
# DataDirectory: /home/narendran/Documents/CEDAR_stuff2/apfel-code-for-photon-pdfs/RUN2/prof_refdata_29
#
# Limits:
#	PDF_APL  	0.000965 0.998800
#	PDF_BPL  	-0.977988 1.974870
#	PDF_AHAD 	0.001407 0.999209
#	PDF_BHAD 	-0.986509 1.998866
#	PDF_AHADG	0.004202 1.996735
#	PDF_BHADG	-0.989979 1.485580
#
# Fixed:
#
# Minimisation result:
#
# GOF 1286.844969
# UNITGOF 1286.844969
# NDOF 237.000000
PDF_APL  	0.291053
PDF_BPL  	-0.371662
PDF_AHAD 	0.221593
PDF_BHAD 	0.071529
PDF_AHADG	1.919762
PDF_BHADG	0.762220
#
# MIGRAD errors:
#
# PDF_APL  	1.495063e-03
# PDF_BPL  	2.292074e-03
# PDF_AHAD 	8.939119e-04
# PDF_BHAD 	2.429667e-03
# PDF_AHADG	9.010222e-04
# PDF_BHADG	2.433192e-03
#
# Correlation matrix:
#
#
#
# Weights used
#
# /H1_2002_I581409/d01-x01-y01      0.6   # 6 bins from 45 to 180
# /H1_2002_I581409/d02-x01-y01      0.6   # 4 bins from 20 to 80
# /H1_2002_I581409/d03-x01-y01      0.6   # 4 bins from 25 to 80
# /H1_2002_I581409/d04-x01-y01      0.6   # 5 bins from 0.6 to 2.5
# /H1_2002_I581409/d04-x01-y02      0.6   # 4 bins from 0.9 to 2.5
# /H1_2002_I581409/d05-x01-y01      0.6   # 6 bins from 0 to 2.5
# /H1_2002_I581409/d05-x01-y02      0.6   # 4 bins from 0.6 to 2.1
# /H1_2002_I581409/d06-x01-y01      0.8   # 3 bins from 0.5 to 1
# /H1_2002_I581409/d06-x01-y02      0.8   # 5 bins from 0.1 to 1
# /H1_2002_I581409/d07-x01-y01      1.0   # 5 bins from 0.05 to 0.6
# /H1_2002_I581409/d07-x01-y02      0.1   # 5 bins from 0.05 to 0.6
# /H1_2002_I581409/d08-x01-y01      0.8   # 5 bins from 0.1 to 1
# /H1_2002_I581409/d08-x01-y02      0.8   # 5 bins from 0.1 to 1
# /H1_2002_I581409/d09-x01-y01      1.0   # 7 bins from 0 to 0.7
# /H1_2002_I581409/d09-x01-y02      0.1   # 7 bins from 0 to 0.7
# /H1_2002_I581409/d10-x01-y01      1.0   # 8 bins from 0 to 0.85
# /H1_2002_I581409/d10-x01-y02      0.1   # 8 bins from 0 to 0.85
# /ZEUS_1997_I450085/d01-x01-y01    0.5   # 11 bins from -1.125 to 1.625
# /ZEUS_1997_I450085/d02-x01-y01    0.5   # 10 bins from -0.875 to 1.625
# /ZEUS_1997_I450085/d03-x01-y01    0.5   # 4 bins from -0.375 to 1.625
# /ZEUS_1997_I450085/d04-x01-y01    0.5   # 4 bins from -0.375 to 1.625
# /ZEUS_1997_I450085/d21-x01-y01    0.1   # 9 bins from -1.125 to 1.125
# /ZEUS_1997_I450085/d22-x01-y01    0.1   # 8 bins from -0.875 to 1.125
# /ZEUS_1997_I450085/d23-x01-y01    0.1   # 4 bins from -0.375 to 1.625
# /ZEUS_1997_I450085/d24-x01-y01    0.1   # 4 bins from -0.375 to 1.625
# /ZEUS_1997_I450085/d25-x01-y01    1.0   # 8 bins from -0.375 to 1.5
# /ZEUS_1997_I450085/d26-x01-y01    1.0   # 8 bins from -0.375 to 1.625
# /ZEUS_1997_I450085/d27-x01-y01    1.0   # 3 bins from 0.125 to 1.625
# /ZEUS_1997_I450085/d28-x01-y01    1.0   # 3 bins from 0.125 to 1.625
# /ZEUS_2012_I1116258/d01-x01-y01   0.6   # 9 bins from 17 to 95
# /ZEUS_2012_I1116258/d02-x01-y01   0.6   # 13 bins from -0.75 to 2.5
# /ZEUS_2012_I1116258/d03-x01-y01   0.6   # 13 bins from -0.75 to 2.5
# /ZEUS_2012_I1116258/d04-x01-y01   0.6   # 5 bins from 17 to 41
# /ZEUS_2012_I1116258/d05-x01-y01   0.6   # 9 bins from 17 to 95
# /ZEUS_2012_I1116258/d06-x01-y01   0.6   # 9 bins from 17 to 95
# /ZEUS_2012_I1116258/d07-x01-y01   0.6   # 9 bins from 17 to 95
# /ZEUS_2012_I1116258/d08-x01-y01   0.6   # 9 bins from 17 to 95
# /ZEUS_2012_I1116258/d09-x01-y01   0.0   # 9 bins from 17 to 95
# /ZEUS_2012_I1116258/d10-x01-y01   0.0   # 9 bins from 17 to 95
# /ZEUS_2012_I1116258/d11-x01-y01   0.0   # 13 bins from -0.75 to 2.5
# /ZEUS_2012_I1116258/d12-x01-y01   0.0   # 13 bins from -0.75 to 2.5
#
# Mean contribution:
#
# /H1_2002_I581409/d01-x01-y01	0.995822
# /H1_2002_I581409/d02-x01-y01	0.454483
# /H1_2002_I581409/d03-x01-y01	1.083280
# /H1_2002_I581409/d04-x01-y01	2.223318
# /H1_2002_I581409/d04-x01-y02	0.954060
# /H1_2002_I581409/d05-x01-y01	4.394178
# /H1_2002_I581409/d05-x01-y02	1.925979
# /H1_2002_I581409/d06-x01-y01	3.876681
# /H1_2002_I581409/d06-x01-y02	12.449784
# /H1_2002_I581409/d07-x01-y01	3.526357
# /H1_2002_I581409/d07-x01-y02	0.071684
# /H1_2002_I581409/d08-x01-y01	9.910137
# /H1_2002_I581409/d08-x01-y02	3.384473
# /H1_2002_I581409/d09-x01-y01	2.175460
# /H1_2002_I581409/d09-x01-y02	0.060377
# /H1_2002_I581409/d10-x01-y01	2.268680
# /H1_2002_I581409/d10-x01-y02	0.042841
# /ZEUS_1997_I450085/d01-x01-y01	4.736850
# /ZEUS_1997_I450085/d02-x01-y01	2.106110
# /ZEUS_1997_I450085/d03-x01-y01	2.172137
# /ZEUS_1997_I450085/d04-x01-y01	0.601490
# /ZEUS_1997_I450085/d21-x01-y01	0.101599
# /ZEUS_1997_I450085/d22-x01-y01	0.040000
# /ZEUS_1997_I450085/d23-x01-y01	0.022143
# /ZEUS_1997_I450085/d24-x01-y01	0.018989
# /ZEUS_1997_I450085/d25-x01-y01	32.757933
# /ZEUS_1997_I450085/d26-x01-y01	21.544944
# /ZEUS_1997_I450085/d27-x01-y01	19.620887
# /ZEUS_1997_I450085/d28-x01-y01	2.046240
# /ZEUS_2012_I1116258/d01-x01-y01	2.792979
# /ZEUS_2012_I1116258/d02-x01-y01	12.847924
# /ZEUS_2012_I1116258/d03-x01-y01	5.774115
# /ZEUS_2012_I1116258/d04-x01-y01	7.704315
# /ZEUS_2012_I1116258/d05-x01-y01	6.293463
# /ZEUS_2012_I1116258/d06-x01-y01	5.294538
# /ZEUS_2012_I1116258/d07-x01-y01	1.187881
# /ZEUS_2012_I1116258/d08-x01-y01	3.046648
