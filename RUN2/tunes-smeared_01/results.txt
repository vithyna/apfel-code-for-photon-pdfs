# ProfVersion: 2.4.2
# Date: 2024-03-11 22:52:03
# InterpolationFile: /home/narendran/Documents/CEDAR_stuff2/apfel-code-for-photon-pdfs/RUN2/ipol.dat
# DataDirectory: /home/narendran/Documents/CEDAR_stuff2/apfel-code-for-photon-pdfs/RUN2/prof_refdata_01
#
# Limits:
#	PDF_APL  	0.000965 0.998800
#	PDF_BPL  	-0.977988 1.974870
#	PDF_AHAD 	0.001407 0.999209
#	PDF_BHAD 	-0.986509 1.998866
#	PDF_AHADG	0.004202 1.996735
#	PDF_BHADG	-0.989979 1.485580
#
# Fixed:
#
# Minimisation result:
#
# GOF 1290.061806
# UNITGOF 1290.061806
# NDOF 237.000000
PDF_APL  	0.282417
PDF_BPL  	-0.361411
PDF_AHAD 	0.216332
PDF_BHAD 	0.084220
PDF_AHADG	1.916742
PDF_BHADG	0.757635
#
# MIGRAD errors:
#
# PDF_APL  	1.115218e-03
# PDF_BPL  	2.402164e-03
# PDF_AHAD 	4.261770e-04
# PDF_BHAD 	2.559388e-03
# PDF_AHADG	1.121315e-03
# PDF_BHADG	3.050943e-03
#
# Correlation matrix:
#
#
#
# Weights used
#
# /H1_2002_I581409/d01-x01-y01      0.6   # 6 bins from 45 to 180
# /H1_2002_I581409/d02-x01-y01      0.6   # 4 bins from 20 to 80
# /H1_2002_I581409/d03-x01-y01      0.6   # 4 bins from 25 to 80
# /H1_2002_I581409/d04-x01-y01      0.6   # 5 bins from 0.6 to 2.5
# /H1_2002_I581409/d04-x01-y02      0.6   # 4 bins from 0.9 to 2.5
# /H1_2002_I581409/d05-x01-y01      0.6   # 6 bins from 0 to 2.5
# /H1_2002_I581409/d05-x01-y02      0.6   # 4 bins from 0.6 to 2.1
# /H1_2002_I581409/d06-x01-y01      0.8   # 3 bins from 0.5 to 1
# /H1_2002_I581409/d06-x01-y02      0.8   # 5 bins from 0.1 to 1
# /H1_2002_I581409/d07-x01-y01      1.0   # 5 bins from 0.05 to 0.6
# /H1_2002_I581409/d07-x01-y02      0.1   # 5 bins from 0.05 to 0.6
# /H1_2002_I581409/d08-x01-y01      0.8   # 5 bins from 0.1 to 1
# /H1_2002_I581409/d08-x01-y02      0.8   # 5 bins from 0.1 to 1
# /H1_2002_I581409/d09-x01-y01      1.0   # 7 bins from 0 to 0.7
# /H1_2002_I581409/d09-x01-y02      0.1   # 7 bins from 0 to 0.7
# /H1_2002_I581409/d10-x01-y01      1.0   # 8 bins from 0 to 0.85
# /H1_2002_I581409/d10-x01-y02      0.1   # 8 bins from 0 to 0.85
# /ZEUS_1997_I450085/d01-x01-y01    0.5   # 11 bins from -1.125 to 1.625
# /ZEUS_1997_I450085/d02-x01-y01    0.5   # 10 bins from -0.875 to 1.625
# /ZEUS_1997_I450085/d03-x01-y01    0.5   # 4 bins from -0.375 to 1.625
# /ZEUS_1997_I450085/d04-x01-y01    0.5   # 4 bins from -0.375 to 1.625
# /ZEUS_1997_I450085/d21-x01-y01    0.1   # 9 bins from -1.125 to 1.125
# /ZEUS_1997_I450085/d22-x01-y01    0.1   # 8 bins from -0.875 to 1.125
# /ZEUS_1997_I450085/d23-x01-y01    0.1   # 4 bins from -0.375 to 1.625
# /ZEUS_1997_I450085/d24-x01-y01    0.1   # 4 bins from -0.375 to 1.625
# /ZEUS_1997_I450085/d25-x01-y01    1.0   # 8 bins from -0.375 to 1.5
# /ZEUS_1997_I450085/d26-x01-y01    1.0   # 8 bins from -0.375 to 1.625
# /ZEUS_1997_I450085/d27-x01-y01    1.0   # 3 bins from 0.125 to 1.625
# /ZEUS_1997_I450085/d28-x01-y01    1.0   # 3 bins from 0.125 to 1.625
# /ZEUS_2012_I1116258/d01-x01-y01   0.6   # 9 bins from 17 to 95
# /ZEUS_2012_I1116258/d02-x01-y01   0.6   # 13 bins from -0.75 to 2.5
# /ZEUS_2012_I1116258/d03-x01-y01   0.6   # 13 bins from -0.75 to 2.5
# /ZEUS_2012_I1116258/d04-x01-y01   0.6   # 5 bins from 17 to 41
# /ZEUS_2012_I1116258/d05-x01-y01   0.6   # 9 bins from 17 to 95
# /ZEUS_2012_I1116258/d06-x01-y01   0.6   # 9 bins from 17 to 95
# /ZEUS_2012_I1116258/d07-x01-y01   0.6   # 9 bins from 17 to 95
# /ZEUS_2012_I1116258/d08-x01-y01   0.6   # 9 bins from 17 to 95
# /ZEUS_2012_I1116258/d09-x01-y01   0.0   # 9 bins from 17 to 95
# /ZEUS_2012_I1116258/d10-x01-y01   0.0   # 9 bins from 17 to 95
# /ZEUS_2012_I1116258/d11-x01-y01   0.0   # 13 bins from -0.75 to 2.5
# /ZEUS_2012_I1116258/d12-x01-y01   0.0   # 13 bins from -0.75 to 2.5
#
# Mean contribution:
#
# /H1_2002_I581409/d01-x01-y01	1.069002
# /H1_2002_I581409/d02-x01-y01	0.686696
# /H1_2002_I581409/d03-x01-y01	0.751544
# /H1_2002_I581409/d04-x01-y01	2.575769
# /H1_2002_I581409/d04-x01-y02	1.684741
# /H1_2002_I581409/d05-x01-y01	4.288235
# /H1_2002_I581409/d05-x01-y02	2.319224
# /H1_2002_I581409/d06-x01-y01	3.946267
# /H1_2002_I581409/d06-x01-y02	8.218055
# /H1_2002_I581409/d07-x01-y01	2.043459
# /H1_2002_I581409/d07-x01-y02	0.039679
# /H1_2002_I581409/d08-x01-y01	9.007638
# /H1_2002_I581409/d08-x01-y02	4.700346
# /H1_2002_I581409/d09-x01-y01	1.544360
# /H1_2002_I581409/d09-x01-y02	0.052601
# /H1_2002_I581409/d10-x01-y01	1.994897
# /H1_2002_I581409/d10-x01-y02	0.070838
# /ZEUS_1997_I450085/d01-x01-y01	4.960601
# /ZEUS_1997_I450085/d02-x01-y01	2.609783
# /ZEUS_1997_I450085/d03-x01-y01	2.987664
# /ZEUS_1997_I450085/d04-x01-y01	0.524641
# /ZEUS_1997_I450085/d21-x01-y01	0.111180
# /ZEUS_1997_I450085/d22-x01-y01	0.044087
# /ZEUS_1997_I450085/d23-x01-y01	0.012346
# /ZEUS_1997_I450085/d24-x01-y01	0.013419
# /ZEUS_1997_I450085/d25-x01-y01	32.648412
# /ZEUS_1997_I450085/d26-x01-y01	22.402245
# /ZEUS_1997_I450085/d27-x01-y01	26.854792
# /ZEUS_1997_I450085/d28-x01-y01	1.787902
# /ZEUS_2012_I1116258/d01-x01-y01	3.143024
# /ZEUS_2012_I1116258/d02-x01-y01	11.361954
# /ZEUS_2012_I1116258/d03-x01-y01	5.372166
# /ZEUS_2012_I1116258/d04-x01-y01	8.729091
# /ZEUS_2012_I1116258/d05-x01-y01	6.896659
# /ZEUS_2012_I1116258/d06-x01-y01	4.527998
# /ZEUS_2012_I1116258/d07-x01-y01	2.547336
# /ZEUS_2012_I1116258/d08-x01-y01	2.883765
