# ProfVersion: 2.4.2
# Date: 2024-03-11 22:52:03
# InterpolationFile: /home/narendran/Documents/CEDAR_stuff2/apfel-code-for-photon-pdfs/RUN2/ipol.dat
# DataDirectory: /home/narendran/Documents/CEDAR_stuff2/apfel-code-for-photon-pdfs/RUN2/prof_refdata_15
#
# Limits:
#	PDF_APL  	0.000965 0.998800
#	PDF_BPL  	-0.977988 1.974870
#	PDF_AHAD 	0.001407 0.999209
#	PDF_BHAD 	-0.986509 1.998866
#	PDF_AHADG	0.004202 1.996735
#	PDF_BHADG	-0.989979 1.485580
#
# Fixed:
#
# Minimisation result:
#
# GOF 1294.944523
# UNITGOF 1294.944523
# NDOF 237.000000
PDF_APL  	0.288002
PDF_BPL  	-0.376261
PDF_AHAD 	0.221305
PDF_BHAD 	0.081121
PDF_AHADG	1.920795
PDF_BHADG	0.759065
#
# MIGRAD errors:
#
# PDF_APL  	4.814003e-04
# PDF_BPL  	1.655649e-03
# PDF_AHAD 	3.879976e-04
# PDF_BHAD 	1.105264e-03
# PDF_AHADG	9.337151e-04
# PDF_BHADG	2.384165e-03
#
# Correlation matrix:
#
#
#
# Weights used
#
# /H1_2002_I581409/d01-x01-y01      0.6   # 6 bins from 45 to 180
# /H1_2002_I581409/d02-x01-y01      0.6   # 4 bins from 20 to 80
# /H1_2002_I581409/d03-x01-y01      0.6   # 4 bins from 25 to 80
# /H1_2002_I581409/d04-x01-y01      0.6   # 5 bins from 0.6 to 2.5
# /H1_2002_I581409/d04-x01-y02      0.6   # 4 bins from 0.9 to 2.5
# /H1_2002_I581409/d05-x01-y01      0.6   # 6 bins from 0 to 2.5
# /H1_2002_I581409/d05-x01-y02      0.6   # 4 bins from 0.6 to 2.1
# /H1_2002_I581409/d06-x01-y01      0.8   # 3 bins from 0.5 to 1
# /H1_2002_I581409/d06-x01-y02      0.8   # 5 bins from 0.1 to 1
# /H1_2002_I581409/d07-x01-y01      1.0   # 5 bins from 0.05 to 0.6
# /H1_2002_I581409/d07-x01-y02      0.1   # 5 bins from 0.05 to 0.6
# /H1_2002_I581409/d08-x01-y01      0.8   # 5 bins from 0.1 to 1
# /H1_2002_I581409/d08-x01-y02      0.8   # 5 bins from 0.1 to 1
# /H1_2002_I581409/d09-x01-y01      1.0   # 7 bins from 0 to 0.7
# /H1_2002_I581409/d09-x01-y02      0.1   # 7 bins from 0 to 0.7
# /H1_2002_I581409/d10-x01-y01      1.0   # 8 bins from 0 to 0.85
# /H1_2002_I581409/d10-x01-y02      0.1   # 8 bins from 0 to 0.85
# /ZEUS_1997_I450085/d01-x01-y01    0.5   # 11 bins from -1.125 to 1.625
# /ZEUS_1997_I450085/d02-x01-y01    0.5   # 10 bins from -0.875 to 1.625
# /ZEUS_1997_I450085/d03-x01-y01    0.5   # 4 bins from -0.375 to 1.625
# /ZEUS_1997_I450085/d04-x01-y01    0.5   # 4 bins from -0.375 to 1.625
# /ZEUS_1997_I450085/d21-x01-y01    0.1   # 9 bins from -1.125 to 1.125
# /ZEUS_1997_I450085/d22-x01-y01    0.1   # 8 bins from -0.875 to 1.125
# /ZEUS_1997_I450085/d23-x01-y01    0.1   # 4 bins from -0.375 to 1.625
# /ZEUS_1997_I450085/d24-x01-y01    0.1   # 4 bins from -0.375 to 1.625
# /ZEUS_1997_I450085/d25-x01-y01    1.0   # 8 bins from -0.375 to 1.5
# /ZEUS_1997_I450085/d26-x01-y01    1.0   # 8 bins from -0.375 to 1.625
# /ZEUS_1997_I450085/d27-x01-y01    1.0   # 3 bins from 0.125 to 1.625
# /ZEUS_1997_I450085/d28-x01-y01    1.0   # 3 bins from 0.125 to 1.625
# /ZEUS_2012_I1116258/d01-x01-y01   0.6   # 9 bins from 17 to 95
# /ZEUS_2012_I1116258/d02-x01-y01   0.6   # 13 bins from -0.75 to 2.5
# /ZEUS_2012_I1116258/d03-x01-y01   0.6   # 13 bins from -0.75 to 2.5
# /ZEUS_2012_I1116258/d04-x01-y01   0.6   # 5 bins from 17 to 41
# /ZEUS_2012_I1116258/d05-x01-y01   0.6   # 9 bins from 17 to 95
# /ZEUS_2012_I1116258/d06-x01-y01   0.6   # 9 bins from 17 to 95
# /ZEUS_2012_I1116258/d07-x01-y01   0.6   # 9 bins from 17 to 95
# /ZEUS_2012_I1116258/d08-x01-y01   0.6   # 9 bins from 17 to 95
# /ZEUS_2012_I1116258/d09-x01-y01   0.0   # 9 bins from 17 to 95
# /ZEUS_2012_I1116258/d10-x01-y01   0.0   # 9 bins from 17 to 95
# /ZEUS_2012_I1116258/d11-x01-y01   0.0   # 13 bins from -0.75 to 2.5
# /ZEUS_2012_I1116258/d12-x01-y01   0.0   # 13 bins from -0.75 to 2.5
#
# Mean contribution:
#
# /H1_2002_I581409/d01-x01-y01	1.087345
# /H1_2002_I581409/d02-x01-y01	0.861016
# /H1_2002_I581409/d03-x01-y01	1.608338
# /H1_2002_I581409/d04-x01-y01	1.809082
# /H1_2002_I581409/d04-x01-y02	1.651715
# /H1_2002_I581409/d05-x01-y01	4.524732
# /H1_2002_I581409/d05-x01-y02	1.817424
# /H1_2002_I581409/d06-x01-y01	3.426461
# /H1_2002_I581409/d06-x01-y02	13.325653
# /H1_2002_I581409/d07-x01-y01	2.086556
# /H1_2002_I581409/d07-x01-y02	0.105958
# /H1_2002_I581409/d08-x01-y01	9.654089
# /H1_2002_I581409/d08-x01-y02	5.183729
# /H1_2002_I581409/d09-x01-y01	1.377986
# /H1_2002_I581409/d09-x01-y02	0.079255
# /H1_2002_I581409/d10-x01-y01	1.346280
# /H1_2002_I581409/d10-x01-y02	0.052990
# /ZEUS_1997_I450085/d01-x01-y01	3.457061
# /ZEUS_1997_I450085/d02-x01-y01	2.211749
# /ZEUS_1997_I450085/d03-x01-y01	2.845062
# /ZEUS_1997_I450085/d04-x01-y01	0.281571
# /ZEUS_1997_I450085/d21-x01-y01	0.138166
# /ZEUS_1997_I450085/d22-x01-y01	0.031111
# /ZEUS_1997_I450085/d23-x01-y01	0.030750
# /ZEUS_1997_I450085/d24-x01-y01	0.032749
# /ZEUS_1997_I450085/d25-x01-y01	28.623964
# /ZEUS_1997_I450085/d26-x01-y01	22.512964
# /ZEUS_1997_I450085/d27-x01-y01	20.449506
# /ZEUS_1997_I450085/d28-x01-y01	2.170360
# /ZEUS_2012_I1116258/d01-x01-y01	2.580867
# /ZEUS_2012_I1116258/d02-x01-y01	13.987115
# /ZEUS_2012_I1116258/d03-x01-y01	5.655712
# /ZEUS_2012_I1116258/d04-x01-y01	7.851178
# /ZEUS_2012_I1116258/d05-x01-y01	7.738695
# /ZEUS_2012_I1116258/d06-x01-y01	5.837887
# /ZEUS_2012_I1116258/d07-x01-y01	2.731486
# /ZEUS_2012_I1116258/d08-x01-y01	3.228042
