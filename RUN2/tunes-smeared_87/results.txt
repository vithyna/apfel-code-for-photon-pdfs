# ProfVersion: 2.4.2
# Date: 2024-03-11 22:57:15
# InterpolationFile: /home/narendran/Documents/CEDAR_stuff2/apfel-code-for-photon-pdfs/RUN2/ipol.dat
# DataDirectory: /home/narendran/Documents/CEDAR_stuff2/apfel-code-for-photon-pdfs/RUN2/prof_refdata_87
#
# Limits:
#	PDF_APL  	0.000965 0.998800
#	PDF_BPL  	-0.977988 1.974870
#	PDF_AHAD 	0.001407 0.999209
#	PDF_BHAD 	-0.986509 1.998866
#	PDF_AHADG	0.004202 1.996735
#	PDF_BHADG	-0.989979 1.485580
#
# Fixed:
#
# Minimisation result:
#
# GOF 1291.607105
# UNITGOF 1291.607105
# NDOF 237.000000
PDF_APL  	0.286903
PDF_BPL  	-0.369331
PDF_AHAD 	0.218802
PDF_BHAD 	0.073741
PDF_AHADG	1.918913
PDF_BHADG	0.758852
#
# MIGRAD errors:
#
# PDF_APL  	1.142688e-03
# PDF_BPL  	1.389117e-03
# PDF_AHAD 	8.686959e-04
# PDF_BHAD 	1.234091e-03
# PDF_AHADG	5.114250e-04
# PDF_BHADG	1.324772e-03
#
# Correlation matrix:
#
#
#
# Weights used
#
# /H1_2002_I581409/d01-x01-y01      0.6   # 6 bins from 45 to 180
# /H1_2002_I581409/d02-x01-y01      0.6   # 4 bins from 20 to 80
# /H1_2002_I581409/d03-x01-y01      0.6   # 4 bins from 25 to 80
# /H1_2002_I581409/d04-x01-y01      0.6   # 5 bins from 0.6 to 2.5
# /H1_2002_I581409/d04-x01-y02      0.6   # 4 bins from 0.9 to 2.5
# /H1_2002_I581409/d05-x01-y01      0.6   # 6 bins from 0 to 2.5
# /H1_2002_I581409/d05-x01-y02      0.6   # 4 bins from 0.6 to 2.1
# /H1_2002_I581409/d06-x01-y01      0.8   # 3 bins from 0.5 to 1
# /H1_2002_I581409/d06-x01-y02      0.8   # 5 bins from 0.1 to 1
# /H1_2002_I581409/d07-x01-y01      1.0   # 5 bins from 0.05 to 0.6
# /H1_2002_I581409/d07-x01-y02      0.1   # 5 bins from 0.05 to 0.6
# /H1_2002_I581409/d08-x01-y01      0.8   # 5 bins from 0.1 to 1
# /H1_2002_I581409/d08-x01-y02      0.8   # 5 bins from 0.1 to 1
# /H1_2002_I581409/d09-x01-y01      1.0   # 7 bins from 0 to 0.7
# /H1_2002_I581409/d09-x01-y02      0.1   # 7 bins from 0 to 0.7
# /H1_2002_I581409/d10-x01-y01      1.0   # 8 bins from 0 to 0.85
# /H1_2002_I581409/d10-x01-y02      0.1   # 8 bins from 0 to 0.85
# /ZEUS_1997_I450085/d01-x01-y01    0.5   # 11 bins from -1.125 to 1.625
# /ZEUS_1997_I450085/d02-x01-y01    0.5   # 10 bins from -0.875 to 1.625
# /ZEUS_1997_I450085/d03-x01-y01    0.5   # 4 bins from -0.375 to 1.625
# /ZEUS_1997_I450085/d04-x01-y01    0.5   # 4 bins from -0.375 to 1.625
# /ZEUS_1997_I450085/d21-x01-y01    0.1   # 9 bins from -1.125 to 1.125
# /ZEUS_1997_I450085/d22-x01-y01    0.1   # 8 bins from -0.875 to 1.125
# /ZEUS_1997_I450085/d23-x01-y01    0.1   # 4 bins from -0.375 to 1.625
# /ZEUS_1997_I450085/d24-x01-y01    0.1   # 4 bins from -0.375 to 1.625
# /ZEUS_1997_I450085/d25-x01-y01    1.0   # 8 bins from -0.375 to 1.5
# /ZEUS_1997_I450085/d26-x01-y01    1.0   # 8 bins from -0.375 to 1.625
# /ZEUS_1997_I450085/d27-x01-y01    1.0   # 3 bins from 0.125 to 1.625
# /ZEUS_1997_I450085/d28-x01-y01    1.0   # 3 bins from 0.125 to 1.625
# /ZEUS_2012_I1116258/d01-x01-y01   0.6   # 9 bins from 17 to 95
# /ZEUS_2012_I1116258/d02-x01-y01   0.6   # 13 bins from -0.75 to 2.5
# /ZEUS_2012_I1116258/d03-x01-y01   0.6   # 13 bins from -0.75 to 2.5
# /ZEUS_2012_I1116258/d04-x01-y01   0.6   # 5 bins from 17 to 41
# /ZEUS_2012_I1116258/d05-x01-y01   0.6   # 9 bins from 17 to 95
# /ZEUS_2012_I1116258/d06-x01-y01   0.6   # 9 bins from 17 to 95
# /ZEUS_2012_I1116258/d07-x01-y01   0.6   # 9 bins from 17 to 95
# /ZEUS_2012_I1116258/d08-x01-y01   0.6   # 9 bins from 17 to 95
# /ZEUS_2012_I1116258/d09-x01-y01   0.0   # 9 bins from 17 to 95
# /ZEUS_2012_I1116258/d10-x01-y01   0.0   # 9 bins from 17 to 95
# /ZEUS_2012_I1116258/d11-x01-y01   0.0   # 13 bins from -0.75 to 2.5
# /ZEUS_2012_I1116258/d12-x01-y01   0.0   # 13 bins from -0.75 to 2.5
#
# Mean contribution:
#
# /H1_2002_I581409/d01-x01-y01	1.122838
# /H1_2002_I581409/d02-x01-y01	1.046017
# /H1_2002_I581409/d03-x01-y01	0.969754
# /H1_2002_I581409/d04-x01-y01	1.908181
# /H1_2002_I581409/d04-x01-y02	1.115565
# /H1_2002_I581409/d05-x01-y01	4.774770
# /H1_2002_I581409/d05-x01-y02	1.123643
# /H1_2002_I581409/d06-x01-y01	4.848116
# /H1_2002_I581409/d06-x01-y02	10.249331
# /H1_2002_I581409/d07-x01-y01	1.993538
# /H1_2002_I581409/d07-x01-y02	0.070748
# /H1_2002_I581409/d08-x01-y01	8.044921
# /H1_2002_I581409/d08-x01-y02	4.706426
# /H1_2002_I581409/d09-x01-y01	1.371250
# /H1_2002_I581409/d09-x01-y02	0.056482
# /H1_2002_I581409/d10-x01-y01	0.894289
# /H1_2002_I581409/d10-x01-y02	0.032718
# /ZEUS_1997_I450085/d01-x01-y01	4.142197
# /ZEUS_1997_I450085/d02-x01-y01	2.066811
# /ZEUS_1997_I450085/d03-x01-y01	2.613919
# /ZEUS_1997_I450085/d04-x01-y01	0.452862
# /ZEUS_1997_I450085/d21-x01-y01	0.146892
# /ZEUS_1997_I450085/d22-x01-y01	0.065707
# /ZEUS_1997_I450085/d23-x01-y01	0.026301
# /ZEUS_1997_I450085/d24-x01-y01	0.047508
# /ZEUS_1997_I450085/d25-x01-y01	31.100842
# /ZEUS_1997_I450085/d26-x01-y01	23.756424
# /ZEUS_1997_I450085/d27-x01-y01	24.514225
# /ZEUS_1997_I450085/d28-x01-y01	2.014207
# /ZEUS_2012_I1116258/d01-x01-y01	3.036136
# /ZEUS_2012_I1116258/d02-x01-y01	14.556474
# /ZEUS_2012_I1116258/d03-x01-y01	4.941730
# /ZEUS_2012_I1116258/d04-x01-y01	8.795866
# /ZEUS_2012_I1116258/d05-x01-y01	6.622707
# /ZEUS_2012_I1116258/d06-x01-y01	5.248728
# /ZEUS_2012_I1116258/d07-x01-y01	1.697990
# /ZEUS_2012_I1116258/d08-x01-y01	2.932166
