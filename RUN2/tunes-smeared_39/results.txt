# ProfVersion: 2.4.2
# Date: 2024-03-11 22:52:06
# InterpolationFile: /home/narendran/Documents/CEDAR_stuff2/apfel-code-for-photon-pdfs/RUN2/ipol.dat
# DataDirectory: /home/narendran/Documents/CEDAR_stuff2/apfel-code-for-photon-pdfs/RUN2/prof_refdata_39
#
# Limits:
#	PDF_APL  	0.000965 0.998800
#	PDF_BPL  	-0.977988 1.974870
#	PDF_AHAD 	0.001407 0.999209
#	PDF_BHAD 	-0.986509 1.998866
#	PDF_AHADG	0.004202 1.996735
#	PDF_BHADG	-0.989979 1.485580
#
# Fixed:
#
# Minimisation result:
#
# GOF 1221.358379
# UNITGOF 1221.358379
# NDOF 237.000000
PDF_APL  	0.268468
PDF_BPL  	-0.340904
PDF_AHAD 	0.204681
PDF_BHAD 	0.089540
PDF_AHADG	1.911933
PDF_BHADG	0.748347
#
# MIGRAD errors:
#
# PDF_APL  	1.072291e-03
# PDF_BPL  	1.655187e-03
# PDF_AHAD 	7.195671e-04
# PDF_BHAD 	1.585874e-03
# PDF_AHADG	8.536130e-04
# PDF_BHADG	2.071259e-03
#
# Correlation matrix:
#
#
#
# Weights used
#
# /H1_2002_I581409/d01-x01-y01      0.6   # 6 bins from 45 to 180
# /H1_2002_I581409/d02-x01-y01      0.6   # 4 bins from 20 to 80
# /H1_2002_I581409/d03-x01-y01      0.6   # 4 bins from 25 to 80
# /H1_2002_I581409/d04-x01-y01      0.6   # 5 bins from 0.6 to 2.5
# /H1_2002_I581409/d04-x01-y02      0.6   # 4 bins from 0.9 to 2.5
# /H1_2002_I581409/d05-x01-y01      0.6   # 6 bins from 0 to 2.5
# /H1_2002_I581409/d05-x01-y02      0.6   # 4 bins from 0.6 to 2.1
# /H1_2002_I581409/d06-x01-y01      0.8   # 3 bins from 0.5 to 1
# /H1_2002_I581409/d06-x01-y02      0.8   # 5 bins from 0.1 to 1
# /H1_2002_I581409/d07-x01-y01      1.0   # 5 bins from 0.05 to 0.6
# /H1_2002_I581409/d07-x01-y02      0.1   # 5 bins from 0.05 to 0.6
# /H1_2002_I581409/d08-x01-y01      0.8   # 5 bins from 0.1 to 1
# /H1_2002_I581409/d08-x01-y02      0.8   # 5 bins from 0.1 to 1
# /H1_2002_I581409/d09-x01-y01      1.0   # 7 bins from 0 to 0.7
# /H1_2002_I581409/d09-x01-y02      0.1   # 7 bins from 0 to 0.7
# /H1_2002_I581409/d10-x01-y01      1.0   # 8 bins from 0 to 0.85
# /H1_2002_I581409/d10-x01-y02      0.1   # 8 bins from 0 to 0.85
# /ZEUS_1997_I450085/d01-x01-y01    0.5   # 11 bins from -1.125 to 1.625
# /ZEUS_1997_I450085/d02-x01-y01    0.5   # 10 bins from -0.875 to 1.625
# /ZEUS_1997_I450085/d03-x01-y01    0.5   # 4 bins from -0.375 to 1.625
# /ZEUS_1997_I450085/d04-x01-y01    0.5   # 4 bins from -0.375 to 1.625
# /ZEUS_1997_I450085/d21-x01-y01    0.1   # 9 bins from -1.125 to 1.125
# /ZEUS_1997_I450085/d22-x01-y01    0.1   # 8 bins from -0.875 to 1.125
# /ZEUS_1997_I450085/d23-x01-y01    0.1   # 4 bins from -0.375 to 1.625
# /ZEUS_1997_I450085/d24-x01-y01    0.1   # 4 bins from -0.375 to 1.625
# /ZEUS_1997_I450085/d25-x01-y01    1.0   # 8 bins from -0.375 to 1.5
# /ZEUS_1997_I450085/d26-x01-y01    1.0   # 8 bins from -0.375 to 1.625
# /ZEUS_1997_I450085/d27-x01-y01    1.0   # 3 bins from 0.125 to 1.625
# /ZEUS_1997_I450085/d28-x01-y01    1.0   # 3 bins from 0.125 to 1.625
# /ZEUS_2012_I1116258/d01-x01-y01   0.6   # 9 bins from 17 to 95
# /ZEUS_2012_I1116258/d02-x01-y01   0.6   # 13 bins from -0.75 to 2.5
# /ZEUS_2012_I1116258/d03-x01-y01   0.6   # 13 bins from -0.75 to 2.5
# /ZEUS_2012_I1116258/d04-x01-y01   0.6   # 5 bins from 17 to 41
# /ZEUS_2012_I1116258/d05-x01-y01   0.6   # 9 bins from 17 to 95
# /ZEUS_2012_I1116258/d06-x01-y01   0.6   # 9 bins from 17 to 95
# /ZEUS_2012_I1116258/d07-x01-y01   0.6   # 9 bins from 17 to 95
# /ZEUS_2012_I1116258/d08-x01-y01   0.6   # 9 bins from 17 to 95
# /ZEUS_2012_I1116258/d09-x01-y01   0.0   # 9 bins from 17 to 95
# /ZEUS_2012_I1116258/d10-x01-y01   0.0   # 9 bins from 17 to 95
# /ZEUS_2012_I1116258/d11-x01-y01   0.0   # 13 bins from -0.75 to 2.5
# /ZEUS_2012_I1116258/d12-x01-y01   0.0   # 13 bins from -0.75 to 2.5
#
# Mean contribution:
#
# /H1_2002_I581409/d01-x01-y01	1.443989
# /H1_2002_I581409/d02-x01-y01	1.125399
# /H1_2002_I581409/d03-x01-y01	0.952172
# /H1_2002_I581409/d04-x01-y01	1.563394
# /H1_2002_I581409/d04-x01-y02	1.221621
# /H1_2002_I581409/d05-x01-y01	3.942707
# /H1_2002_I581409/d05-x01-y02	1.848006
# /H1_2002_I581409/d06-x01-y01	3.696307
# /H1_2002_I581409/d06-x01-y02	9.785392
# /H1_2002_I581409/d07-x01-y01	2.313484
# /H1_2002_I581409/d07-x01-y02	0.081720
# /H1_2002_I581409/d08-x01-y01	7.794665
# /H1_2002_I581409/d08-x01-y02	4.240175
# /H1_2002_I581409/d09-x01-y01	0.812785
# /H1_2002_I581409/d09-x01-y02	0.058289
# /H1_2002_I581409/d10-x01-y01	2.171525
# /H1_2002_I581409/d10-x01-y02	0.066721
# /ZEUS_1997_I450085/d01-x01-y01	4.216386
# /ZEUS_1997_I450085/d02-x01-y01	2.419141
# /ZEUS_1997_I450085/d03-x01-y01	2.021746
# /ZEUS_1997_I450085/d04-x01-y01	0.399140
# /ZEUS_1997_I450085/d21-x01-y01	0.101562
# /ZEUS_1997_I450085/d22-x01-y01	0.043750
# /ZEUS_1997_I450085/d23-x01-y01	0.005976
# /ZEUS_1997_I450085/d24-x01-y01	0.031814
# /ZEUS_1997_I450085/d25-x01-y01	29.628802
# /ZEUS_1997_I450085/d26-x01-y01	20.013698
# /ZEUS_1997_I450085/d27-x01-y01	26.325701
# /ZEUS_1997_I450085/d28-x01-y01	0.795410
# /ZEUS_2012_I1116258/d01-x01-y01	3.469879
# /ZEUS_2012_I1116258/d02-x01-y01	12.850677
# /ZEUS_2012_I1116258/d03-x01-y01	4.623232
# /ZEUS_2012_I1116258/d04-x01-y01	8.996724
# /ZEUS_2012_I1116258/d05-x01-y01	6.532285
# /ZEUS_2012_I1116258/d06-x01-y01	4.260850
# /ZEUS_2012_I1116258/d07-x01-y01	2.442373
# /ZEUS_2012_I1116258/d08-x01-y01	2.421797
