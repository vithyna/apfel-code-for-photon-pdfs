# ProfVersion: 2.4.2
# Date: 2024-03-11 22:51:56
# InterpolationFile: /home/narendran/Documents/CEDAR_stuff2/apfel-code-for-photon-pdfs/RUN2/ipol.dat
# DataDirectory: /home/narendran/Documents/CEDAR_stuff2/apfel-code-for-photon-pdfs/RUN2/prof_refdata_22
#
# Limits:
#	PDF_APL  	0.000965 0.998800
#	PDF_BPL  	-0.977988 1.974870
#	PDF_AHAD 	0.001407 0.999209
#	PDF_BHAD 	-0.986509 1.998866
#	PDF_AHADG	0.004202 1.996735
#	PDF_BHADG	-0.989979 1.485580
#
# Fixed:
#
# Minimisation result:
#
# GOF 1837.072196
# UNITGOF 1837.072196
# NDOF 237.000000
PDF_APL  	0.417123
PDF_BPL  	-0.518320
PDF_AHAD 	0.297028
PDF_BHAD 	-0.097500
PDF_AHADG	1.959796
PDF_BHADG	0.673554
#
# MIGRAD errors:
#
# PDF_APL  	3.421816e-04
# PDF_BPL  	1.503477e-04
# PDF_AHAD 	4.626875e-04
# PDF_BHAD 	1.479506e-04
# PDF_AHADG	1.212140e-04
# PDF_BHADG	1.210253e-03
#
# Correlation matrix:
#
#
#
# Weights used
#
# /H1_2002_I581409/d01-x01-y01      0.6   # 6 bins from 45 to 180
# /H1_2002_I581409/d02-x01-y01      0.6   # 4 bins from 20 to 80
# /H1_2002_I581409/d03-x01-y01      0.6   # 4 bins from 25 to 80
# /H1_2002_I581409/d04-x01-y01      0.6   # 5 bins from 0.6 to 2.5
# /H1_2002_I581409/d04-x01-y02      0.6   # 4 bins from 0.9 to 2.5
# /H1_2002_I581409/d05-x01-y01      0.6   # 6 bins from 0 to 2.5
# /H1_2002_I581409/d05-x01-y02      0.6   # 4 bins from 0.6 to 2.1
# /H1_2002_I581409/d06-x01-y01      0.8   # 3 bins from 0.5 to 1
# /H1_2002_I581409/d06-x01-y02      0.8   # 5 bins from 0.1 to 1
# /H1_2002_I581409/d07-x01-y01      1.0   # 5 bins from 0.05 to 0.6
# /H1_2002_I581409/d07-x01-y02      0.1   # 5 bins from 0.05 to 0.6
# /H1_2002_I581409/d08-x01-y01      0.8   # 5 bins from 0.1 to 1
# /H1_2002_I581409/d08-x01-y02      0.8   # 5 bins from 0.1 to 1
# /H1_2002_I581409/d09-x01-y01      1.0   # 7 bins from 0 to 0.7
# /H1_2002_I581409/d09-x01-y02      0.1   # 7 bins from 0 to 0.7
# /H1_2002_I581409/d10-x01-y01      1.0   # 8 bins from 0 to 0.85
# /H1_2002_I581409/d10-x01-y02      0.1   # 8 bins from 0 to 0.85
# /ZEUS_1997_I450085/d01-x01-y01    0.5   # 11 bins from -1.125 to 1.625
# /ZEUS_1997_I450085/d02-x01-y01    0.5   # 10 bins from -0.875 to 1.625
# /ZEUS_1997_I450085/d03-x01-y01    0.5   # 4 bins from -0.375 to 1.625
# /ZEUS_1997_I450085/d04-x01-y01    0.5   # 4 bins from -0.375 to 1.625
# /ZEUS_1997_I450085/d21-x01-y01    0.1   # 9 bins from -1.125 to 1.125
# /ZEUS_1997_I450085/d22-x01-y01    0.1   # 8 bins from -0.875 to 1.125
# /ZEUS_1997_I450085/d23-x01-y01    0.1   # 4 bins from -0.375 to 1.625
# /ZEUS_1997_I450085/d24-x01-y01    0.1   # 4 bins from -0.375 to 1.625
# /ZEUS_1997_I450085/d25-x01-y01    1.0   # 8 bins from -0.375 to 1.5
# /ZEUS_1997_I450085/d26-x01-y01    1.0   # 8 bins from -0.375 to 1.625
# /ZEUS_1997_I450085/d27-x01-y01    1.0   # 3 bins from 0.125 to 1.625
# /ZEUS_1997_I450085/d28-x01-y01    1.0   # 3 bins from 0.125 to 1.625
# /ZEUS_2012_I1116258/d01-x01-y01   0.6   # 9 bins from 17 to 95
# /ZEUS_2012_I1116258/d02-x01-y01   0.6   # 13 bins from -0.75 to 2.5
# /ZEUS_2012_I1116258/d03-x01-y01   0.6   # 13 bins from -0.75 to 2.5
# /ZEUS_2012_I1116258/d04-x01-y01   0.6   # 5 bins from 17 to 41
# /ZEUS_2012_I1116258/d05-x01-y01   0.6   # 9 bins from 17 to 95
# /ZEUS_2012_I1116258/d06-x01-y01   0.6   # 9 bins from 17 to 95
# /ZEUS_2012_I1116258/d07-x01-y01   0.6   # 9 bins from 17 to 95
# /ZEUS_2012_I1116258/d08-x01-y01   0.6   # 9 bins from 17 to 95
# /ZEUS_2012_I1116258/d09-x01-y01   0.0   # 9 bins from 17 to 95
# /ZEUS_2012_I1116258/d10-x01-y01   0.0   # 9 bins from 17 to 95
# /ZEUS_2012_I1116258/d11-x01-y01   0.0   # 13 bins from -0.75 to 2.5
# /ZEUS_2012_I1116258/d12-x01-y01   0.0   # 13 bins from -0.75 to 2.5
#
# Mean contribution:
#
# /H1_2002_I581409/d01-x01-y01	0.817246
# /H1_2002_I581409/d02-x01-y01	0.639257
# /H1_2002_I581409/d03-x01-y01	1.121305
# /H1_2002_I581409/d04-x01-y01	2.756111
# /H1_2002_I581409/d04-x01-y02	0.817862
# /H1_2002_I581409/d05-x01-y01	6.422658
# /H1_2002_I581409/d05-x01-y02	2.139111
# /H1_2002_I581409/d06-x01-y01	5.563924
# /H1_2002_I581409/d06-x01-y02	16.778087
# /H1_2002_I581409/d07-x01-y01	4.951955
# /H1_2002_I581409/d07-x01-y02	0.095158
# /H1_2002_I581409/d08-x01-y01	13.914184
# /H1_2002_I581409/d08-x01-y02	5.234339
# /H1_2002_I581409/d09-x01-y01	0.853494
# /H1_2002_I581409/d09-x01-y02	0.058751
# /H1_2002_I581409/d10-x01-y01	2.983621
# /H1_2002_I581409/d10-x01-y02	0.051105
# /ZEUS_1997_I450085/d01-x01-y01	7.324091
# /ZEUS_1997_I450085/d02-x01-y01	3.014778
# /ZEUS_1997_I450085/d03-x01-y01	2.102901
# /ZEUS_1997_I450085/d04-x01-y01	0.594523
# /ZEUS_1997_I450085/d21-x01-y01	0.112495
# /ZEUS_1997_I450085/d22-x01-y01	0.048375
# /ZEUS_1997_I450085/d23-x01-y01	0.018924
# /ZEUS_1997_I450085/d24-x01-y01	0.032618
# /ZEUS_1997_I450085/d25-x01-y01	48.483647
# /ZEUS_1997_I450085/d26-x01-y01	32.512333
# /ZEUS_1997_I450085/d27-x01-y01	16.763207
# /ZEUS_1997_I450085/d28-x01-y01	2.589173
# /ZEUS_2012_I1116258/d01-x01-y01	3.920593
# /ZEUS_2012_I1116258/d02-x01-y01	18.341463
# /ZEUS_2012_I1116258/d03-x01-y01	7.978935
# /ZEUS_2012_I1116258/d04-x01-y01	11.540788
# /ZEUS_2012_I1116258/d05-x01-y01	7.025498
# /ZEUS_2012_I1116258/d06-x01-y01	6.517366
# /ZEUS_2012_I1116258/d07-x01-y01	3.079146
# /ZEUS_2012_I1116258/d08-x01-y01	10.541395
