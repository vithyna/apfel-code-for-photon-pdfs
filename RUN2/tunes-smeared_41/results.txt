# ProfVersion: 2.4.2
# Date: 2024-03-11 22:52:02
# InterpolationFile: /home/narendran/Documents/CEDAR_stuff2/apfel-code-for-photon-pdfs/RUN2/ipol.dat
# DataDirectory: /home/narendran/Documents/CEDAR_stuff2/apfel-code-for-photon-pdfs/RUN2/prof_refdata_41
#
# Limits:
#	PDF_APL  	0.000965 0.998800
#	PDF_BPL  	-0.977988 1.974870
#	PDF_AHAD 	0.001407 0.999209
#	PDF_BHAD 	-0.986509 1.998866
#	PDF_AHADG	0.004202 1.996735
#	PDF_BHADG	-0.989979 1.485580
#
# Fixed:
#
# Minimisation result:
#
# GOF 1271.673000
# UNITGOF 1271.673000
# NDOF 237.000000
PDF_APL  	0.286023
PDF_BPL  	-0.376085
PDF_AHAD 	0.220518
PDF_BHAD 	0.091232
PDF_AHADG	1.922021
PDF_BHADG	0.760488
#
# MIGRAD errors:
#
# PDF_APL  	7.180872e-04
# PDF_BPL  	1.542322e-03
# PDF_AHAD 	4.152429e-04
# PDF_BHAD 	1.481060e-03
# PDF_AHADG	7.373436e-04
# PDF_BHADG	2.059420e-03
#
# Correlation matrix:
#
#
#
# Weights used
#
# /H1_2002_I581409/d01-x01-y01      0.6   # 6 bins from 45 to 180
# /H1_2002_I581409/d02-x01-y01      0.6   # 4 bins from 20 to 80
# /H1_2002_I581409/d03-x01-y01      0.6   # 4 bins from 25 to 80
# /H1_2002_I581409/d04-x01-y01      0.6   # 5 bins from 0.6 to 2.5
# /H1_2002_I581409/d04-x01-y02      0.6   # 4 bins from 0.9 to 2.5
# /H1_2002_I581409/d05-x01-y01      0.6   # 6 bins from 0 to 2.5
# /H1_2002_I581409/d05-x01-y02      0.6   # 4 bins from 0.6 to 2.1
# /H1_2002_I581409/d06-x01-y01      0.8   # 3 bins from 0.5 to 1
# /H1_2002_I581409/d06-x01-y02      0.8   # 5 bins from 0.1 to 1
# /H1_2002_I581409/d07-x01-y01      1.0   # 5 bins from 0.05 to 0.6
# /H1_2002_I581409/d07-x01-y02      0.1   # 5 bins from 0.05 to 0.6
# /H1_2002_I581409/d08-x01-y01      0.8   # 5 bins from 0.1 to 1
# /H1_2002_I581409/d08-x01-y02      0.8   # 5 bins from 0.1 to 1
# /H1_2002_I581409/d09-x01-y01      1.0   # 7 bins from 0 to 0.7
# /H1_2002_I581409/d09-x01-y02      0.1   # 7 bins from 0 to 0.7
# /H1_2002_I581409/d10-x01-y01      1.0   # 8 bins from 0 to 0.85
# /H1_2002_I581409/d10-x01-y02      0.1   # 8 bins from 0 to 0.85
# /ZEUS_1997_I450085/d01-x01-y01    0.5   # 11 bins from -1.125 to 1.625
# /ZEUS_1997_I450085/d02-x01-y01    0.5   # 10 bins from -0.875 to 1.625
# /ZEUS_1997_I450085/d03-x01-y01    0.5   # 4 bins from -0.375 to 1.625
# /ZEUS_1997_I450085/d04-x01-y01    0.5   # 4 bins from -0.375 to 1.625
# /ZEUS_1997_I450085/d21-x01-y01    0.1   # 9 bins from -1.125 to 1.125
# /ZEUS_1997_I450085/d22-x01-y01    0.1   # 8 bins from -0.875 to 1.125
# /ZEUS_1997_I450085/d23-x01-y01    0.1   # 4 bins from -0.375 to 1.625
# /ZEUS_1997_I450085/d24-x01-y01    0.1   # 4 bins from -0.375 to 1.625
# /ZEUS_1997_I450085/d25-x01-y01    1.0   # 8 bins from -0.375 to 1.5
# /ZEUS_1997_I450085/d26-x01-y01    1.0   # 8 bins from -0.375 to 1.625
# /ZEUS_1997_I450085/d27-x01-y01    1.0   # 3 bins from 0.125 to 1.625
# /ZEUS_1997_I450085/d28-x01-y01    1.0   # 3 bins from 0.125 to 1.625
# /ZEUS_2012_I1116258/d01-x01-y01   0.6   # 9 bins from 17 to 95
# /ZEUS_2012_I1116258/d02-x01-y01   0.6   # 13 bins from -0.75 to 2.5
# /ZEUS_2012_I1116258/d03-x01-y01   0.6   # 13 bins from -0.75 to 2.5
# /ZEUS_2012_I1116258/d04-x01-y01   0.6   # 5 bins from 17 to 41
# /ZEUS_2012_I1116258/d05-x01-y01   0.6   # 9 bins from 17 to 95
# /ZEUS_2012_I1116258/d06-x01-y01   0.6   # 9 bins from 17 to 95
# /ZEUS_2012_I1116258/d07-x01-y01   0.6   # 9 bins from 17 to 95
# /ZEUS_2012_I1116258/d08-x01-y01   0.6   # 9 bins from 17 to 95
# /ZEUS_2012_I1116258/d09-x01-y01   0.0   # 9 bins from 17 to 95
# /ZEUS_2012_I1116258/d10-x01-y01   0.0   # 9 bins from 17 to 95
# /ZEUS_2012_I1116258/d11-x01-y01   0.0   # 13 bins from -0.75 to 2.5
# /ZEUS_2012_I1116258/d12-x01-y01   0.0   # 13 bins from -0.75 to 2.5
#
# Mean contribution:
#
# /H1_2002_I581409/d01-x01-y01	1.076174
# /H1_2002_I581409/d02-x01-y01	0.556841
# /H1_2002_I581409/d03-x01-y01	0.482489
# /H1_2002_I581409/d04-x01-y01	1.586876
# /H1_2002_I581409/d04-x01-y02	1.379502
# /H1_2002_I581409/d05-x01-y01	3.771307
# /H1_2002_I581409/d05-x01-y02	1.623234
# /H1_2002_I581409/d06-x01-y01	2.063901
# /H1_2002_I581409/d06-x01-y02	11.129639
# /H1_2002_I581409/d07-x01-y01	1.490228
# /H1_2002_I581409/d07-x01-y02	0.077020
# /H1_2002_I581409/d08-x01-y01	7.332797
# /H1_2002_I581409/d08-x01-y02	5.304980
# /H1_2002_I581409/d09-x01-y01	0.910514
# /H1_2002_I581409/d09-x01-y02	0.060083
# /H1_2002_I581409/d10-x01-y01	2.173947
# /H1_2002_I581409/d10-x01-y02	0.047733
# /ZEUS_1997_I450085/d01-x01-y01	5.301176
# /ZEUS_1997_I450085/d02-x01-y01	2.404618
# /ZEUS_1997_I450085/d03-x01-y01	2.233313
# /ZEUS_1997_I450085/d04-x01-y01	0.390572
# /ZEUS_1997_I450085/d21-x01-y01	0.127318
# /ZEUS_1997_I450085/d22-x01-y01	0.037729
# /ZEUS_1997_I450085/d23-x01-y01	0.023242
# /ZEUS_1997_I450085/d24-x01-y01	0.027312
# /ZEUS_1997_I450085/d25-x01-y01	31.343988
# /ZEUS_1997_I450085/d26-x01-y01	21.207943
# /ZEUS_1997_I450085/d27-x01-y01	19.474070
# /ZEUS_1997_I450085/d28-x01-y01	2.425621
# /ZEUS_2012_I1116258/d01-x01-y01	3.510553
# /ZEUS_2012_I1116258/d02-x01-y01	13.174577
# /ZEUS_2012_I1116258/d03-x01-y01	6.238135
# /ZEUS_2012_I1116258/d04-x01-y01	8.044925
# /ZEUS_2012_I1116258/d05-x01-y01	6.233199
# /ZEUS_2012_I1116258/d06-x01-y01	6.076037
# /ZEUS_2012_I1116258/d07-x01-y01	2.335733
# /ZEUS_2012_I1116258/d08-x01-y01	2.716019
