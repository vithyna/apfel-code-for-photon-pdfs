# ProfVersion: 2.4.2
# Date: 2024-03-11 22:51:49
# InterpolationFile: /home/narendran/Documents/CEDAR_stuff2/apfel-code-for-photon-pdfs/RUN2/ipol.dat
# DataDirectory: /home/narendran/Documents/CEDAR_stuff2/apfel-code-for-photon-pdfs/RUN2/prof_refdata_37
#
# Limits:
#	PDF_APL  	0.000965 0.998800
#	PDF_BPL  	-0.977988 1.974870
#	PDF_AHAD 	0.001407 0.999209
#	PDF_BHAD 	-0.986509 1.998866
#	PDF_AHADG	0.004202 1.996735
#	PDF_BHADG	-0.989979 1.485580
#
# Fixed:
#
# Minimisation result:
#
# GOF 1240.395387
# UNITGOF 1240.395387
# NDOF 237.000000
PDF_APL  	0.270480
PDF_BPL  	-0.354022
PDF_AHAD 	0.208217
PDF_BHAD 	0.099184
PDF_AHADG	1.916414
PDF_BHADG	0.747242
#
# MIGRAD errors:
#
# PDF_APL  	1.153683e-03
# PDF_BPL  	6.568165e-04
# PDF_AHAD 	1.021206e-03
# PDF_BHAD 	7.452566e-04
# PDF_AHADG	3.329190e-04
# PDF_BHADG	7.467346e-04
#
# Correlation matrix:
#
#
#
# Weights used
#
# /H1_2002_I581409/d01-x01-y01      0.6   # 6 bins from 45 to 180
# /H1_2002_I581409/d02-x01-y01      0.6   # 4 bins from 20 to 80
# /H1_2002_I581409/d03-x01-y01      0.6   # 4 bins from 25 to 80
# /H1_2002_I581409/d04-x01-y01      0.6   # 5 bins from 0.6 to 2.5
# /H1_2002_I581409/d04-x01-y02      0.6   # 4 bins from 0.9 to 2.5
# /H1_2002_I581409/d05-x01-y01      0.6   # 6 bins from 0 to 2.5
# /H1_2002_I581409/d05-x01-y02      0.6   # 4 bins from 0.6 to 2.1
# /H1_2002_I581409/d06-x01-y01      0.8   # 3 bins from 0.5 to 1
# /H1_2002_I581409/d06-x01-y02      0.8   # 5 bins from 0.1 to 1
# /H1_2002_I581409/d07-x01-y01      1.0   # 5 bins from 0.05 to 0.6
# /H1_2002_I581409/d07-x01-y02      0.1   # 5 bins from 0.05 to 0.6
# /H1_2002_I581409/d08-x01-y01      0.8   # 5 bins from 0.1 to 1
# /H1_2002_I581409/d08-x01-y02      0.8   # 5 bins from 0.1 to 1
# /H1_2002_I581409/d09-x01-y01      1.0   # 7 bins from 0 to 0.7
# /H1_2002_I581409/d09-x01-y02      0.1   # 7 bins from 0 to 0.7
# /H1_2002_I581409/d10-x01-y01      1.0   # 8 bins from 0 to 0.85
# /H1_2002_I581409/d10-x01-y02      0.1   # 8 bins from 0 to 0.85
# /ZEUS_1997_I450085/d01-x01-y01    0.5   # 11 bins from -1.125 to 1.625
# /ZEUS_1997_I450085/d02-x01-y01    0.5   # 10 bins from -0.875 to 1.625
# /ZEUS_1997_I450085/d03-x01-y01    0.5   # 4 bins from -0.375 to 1.625
# /ZEUS_1997_I450085/d04-x01-y01    0.5   # 4 bins from -0.375 to 1.625
# /ZEUS_1997_I450085/d21-x01-y01    0.1   # 9 bins from -1.125 to 1.125
# /ZEUS_1997_I450085/d22-x01-y01    0.1   # 8 bins from -0.875 to 1.125
# /ZEUS_1997_I450085/d23-x01-y01    0.1   # 4 bins from -0.375 to 1.625
# /ZEUS_1997_I450085/d24-x01-y01    0.1   # 4 bins from -0.375 to 1.625
# /ZEUS_1997_I450085/d25-x01-y01    1.0   # 8 bins from -0.375 to 1.5
# /ZEUS_1997_I450085/d26-x01-y01    1.0   # 8 bins from -0.375 to 1.625
# /ZEUS_1997_I450085/d27-x01-y01    1.0   # 3 bins from 0.125 to 1.625
# /ZEUS_1997_I450085/d28-x01-y01    1.0   # 3 bins from 0.125 to 1.625
# /ZEUS_2012_I1116258/d01-x01-y01   0.6   # 9 bins from 17 to 95
# /ZEUS_2012_I1116258/d02-x01-y01   0.6   # 13 bins from -0.75 to 2.5
# /ZEUS_2012_I1116258/d03-x01-y01   0.6   # 13 bins from -0.75 to 2.5
# /ZEUS_2012_I1116258/d04-x01-y01   0.6   # 5 bins from 17 to 41
# /ZEUS_2012_I1116258/d05-x01-y01   0.6   # 9 bins from 17 to 95
# /ZEUS_2012_I1116258/d06-x01-y01   0.6   # 9 bins from 17 to 95
# /ZEUS_2012_I1116258/d07-x01-y01   0.6   # 9 bins from 17 to 95
# /ZEUS_2012_I1116258/d08-x01-y01   0.6   # 9 bins from 17 to 95
# /ZEUS_2012_I1116258/d09-x01-y01   0.0   # 9 bins from 17 to 95
# /ZEUS_2012_I1116258/d10-x01-y01   0.0   # 9 bins from 17 to 95
# /ZEUS_2012_I1116258/d11-x01-y01   0.0   # 13 bins from -0.75 to 2.5
# /ZEUS_2012_I1116258/d12-x01-y01   0.0   # 13 bins from -0.75 to 2.5
#
# Mean contribution:
#
# /H1_2002_I581409/d01-x01-y01	1.454900
# /H1_2002_I581409/d02-x01-y01	0.908589
# /H1_2002_I581409/d03-x01-y01	0.642973
# /H1_2002_I581409/d04-x01-y01	1.399421
# /H1_2002_I581409/d04-x01-y02	2.053117
# /H1_2002_I581409/d05-x01-y01	4.461051
# /H1_2002_I581409/d05-x01-y02	1.147680
# /H1_2002_I581409/d06-x01-y01	3.541005
# /H1_2002_I581409/d06-x01-y02	10.108870
# /H1_2002_I581409/d07-x01-y01	1.716718
# /H1_2002_I581409/d07-x01-y02	0.049107
# /H1_2002_I581409/d08-x01-y01	9.394860
# /H1_2002_I581409/d08-x01-y02	4.537186
# /H1_2002_I581409/d09-x01-y01	1.000779
# /H1_2002_I581409/d09-x01-y02	0.058273
# /H1_2002_I581409/d10-x01-y01	2.291112
# /H1_2002_I581409/d10-x01-y02	0.044636
# /ZEUS_1997_I450085/d01-x01-y01	4.772930
# /ZEUS_1997_I450085/d02-x01-y01	2.280734
# /ZEUS_1997_I450085/d03-x01-y01	2.167096
# /ZEUS_1997_I450085/d04-x01-y01	0.392374
# /ZEUS_1997_I450085/d21-x01-y01	0.119766
# /ZEUS_1997_I450085/d22-x01-y01	0.032970
# /ZEUS_1997_I450085/d23-x01-y01	0.030443
# /ZEUS_1997_I450085/d24-x01-y01	0.013008
# /ZEUS_1997_I450085/d25-x01-y01	28.974185
# /ZEUS_1997_I450085/d26-x01-y01	19.739362
# /ZEUS_1997_I450085/d27-x01-y01	21.370332
# /ZEUS_1997_I450085/d28-x01-y01	2.095962
# /ZEUS_2012_I1116258/d01-x01-y01	4.020491
# /ZEUS_2012_I1116258/d02-x01-y01	11.365579
# /ZEUS_2012_I1116258/d03-x01-y01	6.348316
# /ZEUS_2012_I1116258/d04-x01-y01	8.277503
# /ZEUS_2012_I1116258/d05-x01-y01	7.270103
# /ZEUS_2012_I1116258/d06-x01-y01	6.532018
# /ZEUS_2012_I1116258/d07-x01-y01	1.720369
# /ZEUS_2012_I1116258/d08-x01-y01	2.045767
