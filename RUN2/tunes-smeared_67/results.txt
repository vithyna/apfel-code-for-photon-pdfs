# ProfVersion: 2.4.2
# Date: 2024-03-11 22:57:09
# InterpolationFile: /home/narendran/Documents/CEDAR_stuff2/apfel-code-for-photon-pdfs/RUN2/ipol.dat
# DataDirectory: /home/narendran/Documents/CEDAR_stuff2/apfel-code-for-photon-pdfs/RUN2/prof_refdata_67
#
# Limits:
#	PDF_APL  	0.000965 0.998800
#	PDF_BPL  	-0.977988 1.974870
#	PDF_AHAD 	0.001407 0.999209
#	PDF_BHAD 	-0.986509 1.998866
#	PDF_AHADG	0.004202 1.996735
#	PDF_BHADG	-0.989979 1.485580
#
# Fixed:
#
# Minimisation result:
#
# GOF 1465.527966
# UNITGOF 1465.527966
# NDOF 237.000000
PDF_APL  	0.315590
PDF_BPL  	-0.423709
PDF_AHAD 	0.243751
PDF_BHAD 	0.086452
PDF_AHADG	1.938151
PDF_BHADG	0.778925
#
# MIGRAD errors:
#
# PDF_APL  	2.247615e-03
# PDF_BPL  	1.999727e-03
# PDF_AHAD 	1.707220e-03
# PDF_BHAD 	2.498970e-03
# PDF_AHADG	4.597250e-04
# PDF_BHADG	1.411571e-03
#
# Correlation matrix:
#
#
#
# Weights used
#
# /H1_2002_I581409/d01-x01-y01      0.6   # 6 bins from 45 to 180
# /H1_2002_I581409/d02-x01-y01      0.6   # 4 bins from 20 to 80
# /H1_2002_I581409/d03-x01-y01      0.6   # 4 bins from 25 to 80
# /H1_2002_I581409/d04-x01-y01      0.6   # 5 bins from 0.6 to 2.5
# /H1_2002_I581409/d04-x01-y02      0.6   # 4 bins from 0.9 to 2.5
# /H1_2002_I581409/d05-x01-y01      0.6   # 6 bins from 0 to 2.5
# /H1_2002_I581409/d05-x01-y02      0.6   # 4 bins from 0.6 to 2.1
# /H1_2002_I581409/d06-x01-y01      0.8   # 3 bins from 0.5 to 1
# /H1_2002_I581409/d06-x01-y02      0.8   # 5 bins from 0.1 to 1
# /H1_2002_I581409/d07-x01-y01      1.0   # 5 bins from 0.05 to 0.6
# /H1_2002_I581409/d07-x01-y02      0.1   # 5 bins from 0.05 to 0.6
# /H1_2002_I581409/d08-x01-y01      0.8   # 5 bins from 0.1 to 1
# /H1_2002_I581409/d08-x01-y02      0.8   # 5 bins from 0.1 to 1
# /H1_2002_I581409/d09-x01-y01      1.0   # 7 bins from 0 to 0.7
# /H1_2002_I581409/d09-x01-y02      0.1   # 7 bins from 0 to 0.7
# /H1_2002_I581409/d10-x01-y01      1.0   # 8 bins from 0 to 0.85
# /H1_2002_I581409/d10-x01-y02      0.1   # 8 bins from 0 to 0.85
# /ZEUS_1997_I450085/d01-x01-y01    0.5   # 11 bins from -1.125 to 1.625
# /ZEUS_1997_I450085/d02-x01-y01    0.5   # 10 bins from -0.875 to 1.625
# /ZEUS_1997_I450085/d03-x01-y01    0.5   # 4 bins from -0.375 to 1.625
# /ZEUS_1997_I450085/d04-x01-y01    0.5   # 4 bins from -0.375 to 1.625
# /ZEUS_1997_I450085/d21-x01-y01    0.1   # 9 bins from -1.125 to 1.125
# /ZEUS_1997_I450085/d22-x01-y01    0.1   # 8 bins from -0.875 to 1.125
# /ZEUS_1997_I450085/d23-x01-y01    0.1   # 4 bins from -0.375 to 1.625
# /ZEUS_1997_I450085/d24-x01-y01    0.1   # 4 bins from -0.375 to 1.625
# /ZEUS_1997_I450085/d25-x01-y01    1.0   # 8 bins from -0.375 to 1.5
# /ZEUS_1997_I450085/d26-x01-y01    1.0   # 8 bins from -0.375 to 1.625
# /ZEUS_1997_I450085/d27-x01-y01    1.0   # 3 bins from 0.125 to 1.625
# /ZEUS_1997_I450085/d28-x01-y01    1.0   # 3 bins from 0.125 to 1.625
# /ZEUS_2012_I1116258/d01-x01-y01   0.6   # 9 bins from 17 to 95
# /ZEUS_2012_I1116258/d02-x01-y01   0.6   # 13 bins from -0.75 to 2.5
# /ZEUS_2012_I1116258/d03-x01-y01   0.6   # 13 bins from -0.75 to 2.5
# /ZEUS_2012_I1116258/d04-x01-y01   0.6   # 5 bins from 17 to 41
# /ZEUS_2012_I1116258/d05-x01-y01   0.6   # 9 bins from 17 to 95
# /ZEUS_2012_I1116258/d06-x01-y01   0.6   # 9 bins from 17 to 95
# /ZEUS_2012_I1116258/d07-x01-y01   0.6   # 9 bins from 17 to 95
# /ZEUS_2012_I1116258/d08-x01-y01   0.6   # 9 bins from 17 to 95
# /ZEUS_2012_I1116258/d09-x01-y01   0.0   # 9 bins from 17 to 95
# /ZEUS_2012_I1116258/d10-x01-y01   0.0   # 9 bins from 17 to 95
# /ZEUS_2012_I1116258/d11-x01-y01   0.0   # 13 bins from -0.75 to 2.5
# /ZEUS_2012_I1116258/d12-x01-y01   0.0   # 13 bins from -0.75 to 2.5
#
# Mean contribution:
#
# /H1_2002_I581409/d01-x01-y01	1.310585
# /H1_2002_I581409/d02-x01-y01	2.292727
# /H1_2002_I581409/d03-x01-y01	0.636344
# /H1_2002_I581409/d04-x01-y01	3.011299
# /H1_2002_I581409/d04-x01-y02	1.717657
# /H1_2002_I581409/d05-x01-y01	3.780305
# /H1_2002_I581409/d05-x01-y02	2.807862
# /H1_2002_I581409/d06-x01-y01	5.192824
# /H1_2002_I581409/d06-x01-y02	12.346066
# /H1_2002_I581409/d07-x01-y01	3.537593
# /H1_2002_I581409/d07-x01-y02	0.079322
# /H1_2002_I581409/d08-x01-y01	7.811782
# /H1_2002_I581409/d08-x01-y02	3.826064
# /H1_2002_I581409/d09-x01-y01	1.356011
# /H1_2002_I581409/d09-x01-y02	0.050001
# /H1_2002_I581409/d10-x01-y01	2.694355
# /H1_2002_I581409/d10-x01-y02	0.048934
# /ZEUS_1997_I450085/d01-x01-y01	5.121426
# /ZEUS_1997_I450085/d02-x01-y01	2.898758
# /ZEUS_1997_I450085/d03-x01-y01	2.324292
# /ZEUS_1997_I450085/d04-x01-y01	0.505119
# /ZEUS_1997_I450085/d21-x01-y01	0.134521
# /ZEUS_1997_I450085/d22-x01-y01	0.074701
# /ZEUS_1997_I450085/d23-x01-y01	0.013243
# /ZEUS_1997_I450085/d24-x01-y01	0.019538
# /ZEUS_1997_I450085/d25-x01-y01	31.676679
# /ZEUS_1997_I450085/d26-x01-y01	23.624520
# /ZEUS_1997_I450085/d27-x01-y01	15.489789
# /ZEUS_1997_I450085/d28-x01-y01	1.081991
# /ZEUS_2012_I1116258/d01-x01-y01	3.317020
# /ZEUS_2012_I1116258/d02-x01-y01	16.188092
# /ZEUS_2012_I1116258/d03-x01-y01	6.685192
# /ZEUS_2012_I1116258/d04-x01-y01	13.050802
# /ZEUS_2012_I1116258/d05-x01-y01	9.804935
# /ZEUS_2012_I1116258/d06-x01-y01	8.828008
# /ZEUS_2012_I1116258/d07-x01-y01	2.752869
# /ZEUS_2012_I1116258/d08-x01-y01	3.233257
