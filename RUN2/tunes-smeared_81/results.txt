# ProfVersion: 2.4.2
# Date: 2024-03-11 22:57:09
# InterpolationFile: /home/narendran/Documents/CEDAR_stuff2/apfel-code-for-photon-pdfs/RUN2/ipol.dat
# DataDirectory: /home/narendran/Documents/CEDAR_stuff2/apfel-code-for-photon-pdfs/RUN2/prof_refdata_81
#
# Limits:
#	PDF_APL  	0.000965 0.998800
#	PDF_BPL  	-0.977988 1.974870
#	PDF_AHAD 	0.001407 0.999209
#	PDF_BHAD 	-0.986509 1.998866
#	PDF_AHADG	0.004202 1.996735
#	PDF_BHADG	-0.989979 1.485580
#
# Fixed:
#
# Minimisation result:
#
# GOF 1550.464942
# UNITGOF 1550.464942
# NDOF 237.000000
PDF_APL  	0.345055
PDF_BPL  	-0.442014
PDF_AHAD 	0.262876
PDF_BHAD 	0.020260
PDF_AHADG	1.932982
PDF_BHADG	0.817722
#
# MIGRAD errors:
#
# PDF_APL  	6.447930e-04
# PDF_BPL  	9.089474e-04
# PDF_AHAD 	8.613344e-04
# PDF_BHAD 	1.358843e-03
# PDF_AHADG	1.518026e-04
# PDF_BHADG	3.657983e-04
#
# Correlation matrix:
#
#
#
# Weights used
#
# /H1_2002_I581409/d01-x01-y01      0.6   # 6 bins from 45 to 180
# /H1_2002_I581409/d02-x01-y01      0.6   # 4 bins from 20 to 80
# /H1_2002_I581409/d03-x01-y01      0.6   # 4 bins from 25 to 80
# /H1_2002_I581409/d04-x01-y01      0.6   # 5 bins from 0.6 to 2.5
# /H1_2002_I581409/d04-x01-y02      0.6   # 4 bins from 0.9 to 2.5
# /H1_2002_I581409/d05-x01-y01      0.6   # 6 bins from 0 to 2.5
# /H1_2002_I581409/d05-x01-y02      0.6   # 4 bins from 0.6 to 2.1
# /H1_2002_I581409/d06-x01-y01      0.8   # 3 bins from 0.5 to 1
# /H1_2002_I581409/d06-x01-y02      0.8   # 5 bins from 0.1 to 1
# /H1_2002_I581409/d07-x01-y01      1.0   # 5 bins from 0.05 to 0.6
# /H1_2002_I581409/d07-x01-y02      0.1   # 5 bins from 0.05 to 0.6
# /H1_2002_I581409/d08-x01-y01      0.8   # 5 bins from 0.1 to 1
# /H1_2002_I581409/d08-x01-y02      0.8   # 5 bins from 0.1 to 1
# /H1_2002_I581409/d09-x01-y01      1.0   # 7 bins from 0 to 0.7
# /H1_2002_I581409/d09-x01-y02      0.1   # 7 bins from 0 to 0.7
# /H1_2002_I581409/d10-x01-y01      1.0   # 8 bins from 0 to 0.85
# /H1_2002_I581409/d10-x01-y02      0.1   # 8 bins from 0 to 0.85
# /ZEUS_1997_I450085/d01-x01-y01    0.5   # 11 bins from -1.125 to 1.625
# /ZEUS_1997_I450085/d02-x01-y01    0.5   # 10 bins from -0.875 to 1.625
# /ZEUS_1997_I450085/d03-x01-y01    0.5   # 4 bins from -0.375 to 1.625
# /ZEUS_1997_I450085/d04-x01-y01    0.5   # 4 bins from -0.375 to 1.625
# /ZEUS_1997_I450085/d21-x01-y01    0.1   # 9 bins from -1.125 to 1.125
# /ZEUS_1997_I450085/d22-x01-y01    0.1   # 8 bins from -0.875 to 1.125
# /ZEUS_1997_I450085/d23-x01-y01    0.1   # 4 bins from -0.375 to 1.625
# /ZEUS_1997_I450085/d24-x01-y01    0.1   # 4 bins from -0.375 to 1.625
# /ZEUS_1997_I450085/d25-x01-y01    1.0   # 8 bins from -0.375 to 1.5
# /ZEUS_1997_I450085/d26-x01-y01    1.0   # 8 bins from -0.375 to 1.625
# /ZEUS_1997_I450085/d27-x01-y01    1.0   # 3 bins from 0.125 to 1.625
# /ZEUS_1997_I450085/d28-x01-y01    1.0   # 3 bins from 0.125 to 1.625
# /ZEUS_2012_I1116258/d01-x01-y01   0.6   # 9 bins from 17 to 95
# /ZEUS_2012_I1116258/d02-x01-y01   0.6   # 13 bins from -0.75 to 2.5
# /ZEUS_2012_I1116258/d03-x01-y01   0.6   # 13 bins from -0.75 to 2.5
# /ZEUS_2012_I1116258/d04-x01-y01   0.6   # 5 bins from 17 to 41
# /ZEUS_2012_I1116258/d05-x01-y01   0.6   # 9 bins from 17 to 95
# /ZEUS_2012_I1116258/d06-x01-y01   0.6   # 9 bins from 17 to 95
# /ZEUS_2012_I1116258/d07-x01-y01   0.6   # 9 bins from 17 to 95
# /ZEUS_2012_I1116258/d08-x01-y01   0.6   # 9 bins from 17 to 95
# /ZEUS_2012_I1116258/d09-x01-y01   0.0   # 9 bins from 17 to 95
# /ZEUS_2012_I1116258/d10-x01-y01   0.0   # 9 bins from 17 to 95
# /ZEUS_2012_I1116258/d11-x01-y01   0.0   # 13 bins from -0.75 to 2.5
# /ZEUS_2012_I1116258/d12-x01-y01   0.0   # 13 bins from -0.75 to 2.5
#
# Mean contribution:
#
# /H1_2002_I581409/d01-x01-y01	1.765560
# /H1_2002_I581409/d02-x01-y01	1.460636
# /H1_2002_I581409/d03-x01-y01	2.141660
# /H1_2002_I581409/d04-x01-y01	2.390269
# /H1_2002_I581409/d04-x01-y02	1.596153
# /H1_2002_I581409/d05-x01-y01	6.098908
# /H1_2002_I581409/d05-x01-y02	3.014588
# /H1_2002_I581409/d06-x01-y01	5.533791
# /H1_2002_I581409/d06-x01-y02	12.370250
# /H1_2002_I581409/d07-x01-y01	5.046670
# /H1_2002_I581409/d07-x01-y02	0.061234
# /H1_2002_I581409/d08-x01-y01	12.978139
# /H1_2002_I581409/d08-x01-y02	5.086345
# /H1_2002_I581409/d09-x01-y01	1.155792
# /H1_2002_I581409/d09-x01-y02	0.070456
# /H1_2002_I581409/d10-x01-y01	1.941143
# /H1_2002_I581409/d10-x01-y02	0.060987
# /ZEUS_1997_I450085/d01-x01-y01	6.132686
# /ZEUS_1997_I450085/d02-x01-y01	2.833089
# /ZEUS_1997_I450085/d03-x01-y01	2.164214
# /ZEUS_1997_I450085/d04-x01-y01	0.168435
# /ZEUS_1997_I450085/d21-x01-y01	0.145281
# /ZEUS_1997_I450085/d22-x01-y01	0.058553
# /ZEUS_1997_I450085/d23-x01-y01	0.063089
# /ZEUS_1997_I450085/d24-x01-y01	0.025668
# /ZEUS_1997_I450085/d25-x01-y01	35.164704
# /ZEUS_1997_I450085/d26-x01-y01	22.569219
# /ZEUS_1997_I450085/d27-x01-y01	16.666685
# /ZEUS_1997_I450085/d28-x01-y01	2.210111
# /ZEUS_2012_I1116258/d01-x01-y01	4.883515
# /ZEUS_2012_I1116258/d02-x01-y01	15.259165
# /ZEUS_2012_I1116258/d03-x01-y01	6.716489
# /ZEUS_2012_I1116258/d04-x01-y01	11.111293
# /ZEUS_2012_I1116258/d05-x01-y01	10.403778
# /ZEUS_2012_I1116258/d06-x01-y01	7.858666
# /ZEUS_2012_I1116258/d07-x01-y01	3.227301
# /ZEUS_2012_I1116258/d08-x01-y01	3.911801
