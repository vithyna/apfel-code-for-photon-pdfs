# ProfVersion: 2.4.2
# Date: 2024-03-11 22:57:11
# InterpolationFile: /home/narendran/Documents/CEDAR_stuff2/apfel-code-for-photon-pdfs/RUN2/ipol.dat
# DataDirectory: /home/narendran/Documents/CEDAR_stuff2/apfel-code-for-photon-pdfs/RUN2/prof_refdata_50
#
# Limits:
#	PDF_APL  	0.000965 0.998800
#	PDF_BPL  	-0.977988 1.974870
#	PDF_AHAD 	0.001407 0.999209
#	PDF_BHAD 	-0.986509 1.998866
#	PDF_AHADG	0.004202 1.996735
#	PDF_BHADG	-0.989979 1.485580
#
# Fixed:
#
# Minimisation result:
#
# GOF 1767.035530
# UNITGOF 1767.035530
# NDOF 237.000000
PDF_APL  	0.412514
PDF_BPL  	-0.517725
PDF_AHAD 	0.290018
PDF_BHAD 	-0.101137
PDF_AHADG	1.961647
PDF_BHADG	0.648562
#
# MIGRAD errors:
#
# PDF_APL  	7.239893e-04
# PDF_BPL  	2.620222e-04
# PDF_AHAD 	9.970588e-04
# PDF_BHAD 	3.446639e-04
# PDF_AHADG	2.631096e-04
# PDF_BHADG	2.629462e-03
#
# Correlation matrix:
#
#
#
# Weights used
#
# /H1_2002_I581409/d01-x01-y01      0.6   # 6 bins from 45 to 180
# /H1_2002_I581409/d02-x01-y01      0.6   # 4 bins from 20 to 80
# /H1_2002_I581409/d03-x01-y01      0.6   # 4 bins from 25 to 80
# /H1_2002_I581409/d04-x01-y01      0.6   # 5 bins from 0.6 to 2.5
# /H1_2002_I581409/d04-x01-y02      0.6   # 4 bins from 0.9 to 2.5
# /H1_2002_I581409/d05-x01-y01      0.6   # 6 bins from 0 to 2.5
# /H1_2002_I581409/d05-x01-y02      0.6   # 4 bins from 0.6 to 2.1
# /H1_2002_I581409/d06-x01-y01      0.8   # 3 bins from 0.5 to 1
# /H1_2002_I581409/d06-x01-y02      0.8   # 5 bins from 0.1 to 1
# /H1_2002_I581409/d07-x01-y01      1.0   # 5 bins from 0.05 to 0.6
# /H1_2002_I581409/d07-x01-y02      0.1   # 5 bins from 0.05 to 0.6
# /H1_2002_I581409/d08-x01-y01      0.8   # 5 bins from 0.1 to 1
# /H1_2002_I581409/d08-x01-y02      0.8   # 5 bins from 0.1 to 1
# /H1_2002_I581409/d09-x01-y01      1.0   # 7 bins from 0 to 0.7
# /H1_2002_I581409/d09-x01-y02      0.1   # 7 bins from 0 to 0.7
# /H1_2002_I581409/d10-x01-y01      1.0   # 8 bins from 0 to 0.85
# /H1_2002_I581409/d10-x01-y02      0.1   # 8 bins from 0 to 0.85
# /ZEUS_1997_I450085/d01-x01-y01    0.5   # 11 bins from -1.125 to 1.625
# /ZEUS_1997_I450085/d02-x01-y01    0.5   # 10 bins from -0.875 to 1.625
# /ZEUS_1997_I450085/d03-x01-y01    0.5   # 4 bins from -0.375 to 1.625
# /ZEUS_1997_I450085/d04-x01-y01    0.5   # 4 bins from -0.375 to 1.625
# /ZEUS_1997_I450085/d21-x01-y01    0.1   # 9 bins from -1.125 to 1.125
# /ZEUS_1997_I450085/d22-x01-y01    0.1   # 8 bins from -0.875 to 1.125
# /ZEUS_1997_I450085/d23-x01-y01    0.1   # 4 bins from -0.375 to 1.625
# /ZEUS_1997_I450085/d24-x01-y01    0.1   # 4 bins from -0.375 to 1.625
# /ZEUS_1997_I450085/d25-x01-y01    1.0   # 8 bins from -0.375 to 1.5
# /ZEUS_1997_I450085/d26-x01-y01    1.0   # 8 bins from -0.375 to 1.625
# /ZEUS_1997_I450085/d27-x01-y01    1.0   # 3 bins from 0.125 to 1.625
# /ZEUS_1997_I450085/d28-x01-y01    1.0   # 3 bins from 0.125 to 1.625
# /ZEUS_2012_I1116258/d01-x01-y01   0.6   # 9 bins from 17 to 95
# /ZEUS_2012_I1116258/d02-x01-y01   0.6   # 13 bins from -0.75 to 2.5
# /ZEUS_2012_I1116258/d03-x01-y01   0.6   # 13 bins from -0.75 to 2.5
# /ZEUS_2012_I1116258/d04-x01-y01   0.6   # 5 bins from 17 to 41
# /ZEUS_2012_I1116258/d05-x01-y01   0.6   # 9 bins from 17 to 95
# /ZEUS_2012_I1116258/d06-x01-y01   0.6   # 9 bins from 17 to 95
# /ZEUS_2012_I1116258/d07-x01-y01   0.6   # 9 bins from 17 to 95
# /ZEUS_2012_I1116258/d08-x01-y01   0.6   # 9 bins from 17 to 95
# /ZEUS_2012_I1116258/d09-x01-y01   0.0   # 9 bins from 17 to 95
# /ZEUS_2012_I1116258/d10-x01-y01   0.0   # 9 bins from 17 to 95
# /ZEUS_2012_I1116258/d11-x01-y01   0.0   # 13 bins from -0.75 to 2.5
# /ZEUS_2012_I1116258/d12-x01-y01   0.0   # 13 bins from -0.75 to 2.5
#
# Mean contribution:
#
# /H1_2002_I581409/d01-x01-y01	1.229172
# /H1_2002_I581409/d02-x01-y01	0.714621
# /H1_2002_I581409/d03-x01-y01	0.510461
# /H1_2002_I581409/d04-x01-y01	2.117330
# /H1_2002_I581409/d04-x01-y02	0.684009
# /H1_2002_I581409/d05-x01-y01	6.911225
# /H1_2002_I581409/d05-x01-y02	2.783214
# /H1_2002_I581409/d06-x01-y01	5.989211
# /H1_2002_I581409/d06-x01-y02	13.331481
# /H1_2002_I581409/d07-x01-y01	6.130485
# /H1_2002_I581409/d07-x01-y02	0.054594
# /H1_2002_I581409/d08-x01-y01	16.054025
# /H1_2002_I581409/d08-x01-y02	4.333369
# /H1_2002_I581409/d09-x01-y01	0.664958
# /H1_2002_I581409/d09-x01-y02	0.047962
# /H1_2002_I581409/d10-x01-y01	3.222157
# /H1_2002_I581409/d10-x01-y02	0.041363
# /ZEUS_1997_I450085/d01-x01-y01	7.296058
# /ZEUS_1997_I450085/d02-x01-y01	3.344554
# /ZEUS_1997_I450085/d03-x01-y01	3.273969
# /ZEUS_1997_I450085/d04-x01-y01	0.809183
# /ZEUS_1997_I450085/d21-x01-y01	0.106574
# /ZEUS_1997_I450085/d22-x01-y01	0.034207
# /ZEUS_1997_I450085/d23-x01-y01	0.030765
# /ZEUS_1997_I450085/d24-x01-y01	0.028624
# /ZEUS_1997_I450085/d25-x01-y01	48.207381
# /ZEUS_1997_I450085/d26-x01-y01	32.508041
# /ZEUS_1997_I450085/d27-x01-y01	12.847921
# /ZEUS_1997_I450085/d28-x01-y01	1.476639
# /ZEUS_2012_I1116258/d01-x01-y01	3.571786
# /ZEUS_2012_I1116258/d02-x01-y01	15.563741
# /ZEUS_2012_I1116258/d03-x01-y01	8.755101
# /ZEUS_2012_I1116258/d04-x01-y01	11.055710
# /ZEUS_2012_I1116258/d05-x01-y01	6.224344
# /ZEUS_2012_I1116258/d06-x01-y01	6.695312
# /ZEUS_2012_I1116258/d07-x01-y01	2.715866
# /ZEUS_2012_I1116258/d08-x01-y01	8.417754
