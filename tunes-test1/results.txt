# ProfVersion: 2.3.3
# Date: 2023-02-23 17:59:07
# InterpolationFile: /home/anarendran/Documents/CEDAR_stuff/apfel-code-for-photon-pdfs/ipol.dat
# DataDirectory: /home/anarendran/Documents/CEDAR_stuff/apfel-code-for-photon-pdfs/prof_refdata
#
# Limits:
#	PDF_APL  	-2.995369 2.956472
#	PDF_BPL  	-2.979907 2.926744
#	PDF_AHAD 	-2.905057 2.973668
#	PDF_BHAD 	-2.934657 2.932102
#	PDF_AHADG	-2.983515 2.852990
#	PDF_BHADG	-2.939184 2.961075
#
# Fixed:
#
# Minimisation result:
#
# GOF 38.345124
# UNITGOF 38.345124
# NDOF 2.000000
PDF_APL  	-1.395442
PDF_BPL  	2.161065
PDF_AHAD 	0.527067
PDF_BHAD 	-1.490622
PDF_AHADG	0.435629
PDF_BHADG	1.218515
#
# MIGRAD errors:
#
# PDF_APL  	2.606077e-02
# PDF_BPL  	8.350087e-02
# PDF_AHAD 	1.655205e-01
# PDF_BHAD 	4.207133e-02
# PDF_AHADG	3.945722e-02
# PDF_BHADG	5.888698e-02
#
# Correlation matrix:
#
# PDF_APL      1.000000e+00    6.514855e-01    4.984361e-01    6.888651e-01   -7.680161e-01    3.673682e-01
# PDF_BPL      6.514855e-01    1.000000e+00    9.722883e-01    6.978870e-02   -9.661155e-01    9.217574e-01
# PDF_AHAD     4.984361e-01    9.722883e-01    1.000000e+00   -9.120594e-02   -9.238854e-01    9.689345e-01
# PDF_BHAD     6.888651e-01    6.978870e-02   -9.120594e-02    1.000000e+00   -1.701640e-01   -2.092279e-01
# PDF_AHADG   -7.680161e-01   -9.661155e-01   -9.238854e-01   -1.701640e-01    1.000000e+00   -8.286578e-01
# PDF_BHADG    3.673682e-01    9.217574e-01    9.689345e-01   -2.092279e-01   -8.286578e-01    1.000000e+00
#
# Covariance matrix:
#
# PDF_APL      6.791858e-04    1.418138e-03    2.151255e-03    7.553266e-04   -7.897773e-04    5.638321e-04
# PDF_BPL      1.418138e-03    6.976516e-03    1.344939e-02    2.452509e-04   -3.184113e-03    4.534087e-03
# PDF_AHAD     2.151255e-03    1.344939e-02    2.742688e-02   -6.355030e-04   -6.037350e-03    9.450103e-03
# PDF_BHAD     7.553266e-04    2.452509e-04   -6.355030e-04    1.770160e-03   -2.824971e-04   -5.184178e-04
# PDF_AHADG   -7.897773e-04   -3.184113e-03   -6.037350e-03   -2.824971e-04    1.556970e-03   -1.925614e-03
# PDF_BHADG    5.638321e-04    4.534087e-03    9.450103e-03   -5.184178e-04   -1.925614e-03    3.468230e-03
#
#
# Weights used
#
# /ZEUS_2001_S4815815/d01-x01-y01   0.0   # 8 bins from 0 to 0.8
# /ZEUS_2001_S4815815/d01-x01-y02   1.0   # 8 bins from 0 to 0.8
# /ZEUS_2001_S4815815/d02-x01-y01   0.0   # 2 bins from 14 to 21
# /ZEUS_2001_S4815815/d03-x01-y01   0.0   # 4 bins from 14 to 29
# /ZEUS_2001_S4815815/d04-x01-y01   0.0   # 7 bins from 14 to 48
# /ZEUS_2001_S4815815/d05-x01-y01   0.0   # 6 bins from 14 to 41
# /ZEUS_2001_S4815815/d06-x01-y01   0.0   # 9 bins from 14 to 65
# /ZEUS_2001_S4815815/d07-x01-y01   0.0   # 10 bins from 14 to 75
# /ZEUS_2001_S4815815/d08-x01-y01   0.0   # 1 bins from 14 to 17
# /ZEUS_2001_S4815815/d09-x01-y01   0.0   # 4 bins from 14 to 29
# /ZEUS_2001_S4815815/d10-x01-y01   0.0   # 5 bins from 14 to 35
# /ZEUS_2001_S4815815/d11-x01-y01   0.0   # 5 bins from 14 to 35
# /ZEUS_2001_S4815815/d12-x01-y01   0.0   # 8 bins from 14 to 55
# /ZEUS_2001_S4815815/d13-x01-y01   0.0   # 9 bins from 14 to 65
# /ZEUS_2001_S4815815/d14-x01-y01   0.0   # 7 bins from -1 to 2.4
# /ZEUS_2001_S4815815/d15-x01-y01   0.0   # 7 bins from -1 to 2.4
# /ZEUS_2001_S4815815/d16-x01-y01   0.0   # 7 bins from -1 to 2.4
# /ZEUS_2001_S4815815/d17-x01-y01   0.0   # 6 bins from -0.5 to 2.4
# /ZEUS_2001_S4815815/d18-x01-y01   0.0   # 7 bins from -1 to 2.4
# /ZEUS_2001_S4815815/d19-x01-y01   0.0   # 7 bins from -1 to 2.4
# /ZEUS_2001_S4815815/d20-x01-y01   0.0   # 8 bins from 0.1 to 1
# /ZEUS_2001_S4815815/d21-x01-y01   0.0   # 8 bins from 0.1 to 1
# /ZEUS_2001_S4815815/d22-x01-y01   0.0   # 4 bins from 0.1 to 1
# /ZEUS_2001_S4815815/d23-x01-y01   0.0   # 4 bins from 0.1 to 1
#
# Mean contribution:
#
# /ZEUS_2001_S4815815/d01-x01-y02	4.793140
