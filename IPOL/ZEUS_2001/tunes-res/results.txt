# ProfVersion: 2.3.3
# Date: 2023-02-24 11:24:11
# InterpolationFile: /home/anarendran/Documents/CEDAR_stuff/apfel-code-for-photon-pdfs/IPOL/ZEUS_2001/ipol.dat
# DataDirectory: /home/anarendran/Documents/CEDAR_stuff/apfel-code-for-photon-pdfs/prof_refdata
#
# Limits:
#	PDF_APL  	-2.950048 2.913137
#	PDF_BPL  	-2.981785 2.923029
#	PDF_AHAD 	-2.981013 2.945382
#	PDF_BHAD 	-2.997881 2.984693
#	PDF_AHADG	-2.999687 2.969022
#	PDF_BHADG	-2.967407 2.990951
#
# Fixed:
#
# Minimisation result:
#
# GOF 1411.046814
# UNITGOF 1411.046814
# NDOF 59.000000
PDF_APL  	-0.327675
PDF_BPL  	1.143575
PDF_AHAD 	1.393618
PDF_BHAD 	0.513119
PDF_AHADG	-1.346023
PDF_BHADG	1.071773
#
# MIGRAD errors:
#
# PDF_APL  	4.226982e-02
# PDF_BPL  	2.351409e-02
# PDF_AHAD 	2.666465e-02
# PDF_BHAD 	8.041147e-02
# PDF_AHADG	6.280283e-02
# PDF_BHADG	5.459297e-02
#
# Correlation matrix:
#
# PDF_APL      1.000000e+00   -5.644326e-01   -6.627511e-02   -8.975964e-01    5.668152e-01    2.850823e-01
# PDF_BPL     -5.644326e-01    1.000000e+00    5.711392e-01    3.945901e-01   -1.490989e-02   -3.626839e-02
# PDF_AHAD    -6.627511e-02    5.711392e-01    1.000000e+00    2.402770e-01    3.205053e-01    7.575869e-01
# PDF_BHAD    -8.975964e-01    3.945901e-01    2.402770e-01    1.000000e+00   -4.398289e-01    9.035033e-02
# PDF_AHADG    5.668152e-01   -1.490989e-02    3.205053e-01   -4.398289e-01    1.000000e+00    3.458898e-01
# PDF_BHADG    2.850823e-01   -3.626839e-02    7.575869e-01    9.035033e-02    3.458898e-01    1.000000e+00
#
# Covariance matrix:
#
# PDF_APL      1.786863e-03   -5.610367e-04   -7.470327e-05   -3.051396e-03    1.504895e-03    6.579310e-04
# PDF_BPL     -5.610367e-04    5.529263e-04    3.581122e-04    7.461940e-04   -2.202050e-05   -4.656144e-05
# PDF_AHAD    -7.470327e-05    3.581122e-04    7.110286e-04    5.152614e-04    5.367821e-04    1.102911e-03
# PDF_BHAD    -3.051396e-03    7.461940e-04    5.152614e-04    6.467611e-03   -2.221646e-03    3.967037e-04
# PDF_AHADG    1.504895e-03   -2.202050e-05    5.367821e-04   -2.221646e-03    3.944922e-03    1.186101e-03
# PDF_BHADG    6.579310e-04   -4.656144e-05    1.102911e-03    3.967037e-04    1.186101e-03    2.980774e-03
#
#
# Weights used
#
# /ZEUS_2001_S4815815/d01-x01-y01   0.0   # 8 bins from 0 to 0.8
# /ZEUS_2001_S4815815/d01-x01-y02   1.0   # 8 bins from 0 to 0.8
# /ZEUS_2001_S4815815/d02-x01-y01   0.0   # 2 bins from 14 to 21
# /ZEUS_2001_S4815815/d03-x01-y01   1.0   # 4 bins from 14 to 29
# /ZEUS_2001_S4815815/d04-x01-y01   1.0   # 7 bins from 14 to 48
# /ZEUS_2001_S4815815/d05-x01-y01   1.0   # 6 bins from 14 to 41
# /ZEUS_2001_S4815815/d06-x01-y01   1.0   # 9 bins from 14 to 65
# /ZEUS_2001_S4815815/d07-x01-y01   1.0   # 10 bins from 14 to 75
# /ZEUS_2001_S4815815/d08-x01-y01   0.0   # 1 bins from 14 to 17
# /ZEUS_2001_S4815815/d09-x01-y01   0.0   # 4 bins from 14 to 29
# /ZEUS_2001_S4815815/d10-x01-y01   0.0   # 5 bins from 14 to 35
# /ZEUS_2001_S4815815/d11-x01-y01   0.0   # 5 bins from 14 to 35
# /ZEUS_2001_S4815815/d12-x01-y01   0.0   # 8 bins from 14 to 55
# /ZEUS_2001_S4815815/d13-x01-y01   0.0   # 9 bins from 14 to 65
# /ZEUS_2001_S4815815/d14-x01-y01   1.0   # 7 bins from -1 to 2.4
# /ZEUS_2001_S4815815/d15-x01-y01   1.0   # 7 bins from -1 to 2.4
# /ZEUS_2001_S4815815/d16-x01-y01   1.0   # 7 bins from -1 to 2.4
# /ZEUS_2001_S4815815/d17-x01-y01   0.0   # 6 bins from -0.5 to 2.4
# /ZEUS_2001_S4815815/d18-x01-y01   0.0   # 7 bins from -1 to 2.4
# /ZEUS_2001_S4815815/d19-x01-y01   0.0   # 7 bins from -1 to 2.4
# /ZEUS_2001_S4815815/d20-x01-y01   0.0   # 8 bins from 0.1 to 1
# /ZEUS_2001_S4815815/d21-x01-y01   0.0   # 8 bins from 0.1 to 1
# /ZEUS_2001_S4815815/d22-x01-y01   0.0   # 4 bins from 0.1 to 1
# /ZEUS_2001_S4815815/d23-x01-y01   0.0   # 4 bins from 0.1 to 1
#
# Mean contribution:
#
# /ZEUS_2001_S4815815/d01-x01-y02	22.701331
# /ZEUS_2001_S4815815/d03-x01-y01	4.065704
# /ZEUS_2001_S4815815/d04-x01-y01	23.912468
# /ZEUS_2001_S4815815/d05-x01-y01	14.591429
# /ZEUS_2001_S4815815/d06-x01-y01	14.607682
# /ZEUS_2001_S4815815/d07-x01-y01	26.042091
# /ZEUS_2001_S4815815/d14-x01-y01	20.474397
# /ZEUS_2001_S4815815/d15-x01-y01	20.894997
# /ZEUS_2001_S4815815/d16-x01-y01	39.537384
