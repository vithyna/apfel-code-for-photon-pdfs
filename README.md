# Apfel Code for generating PDFs

The lhaout code here is the starting point for generating PDFs 

## Compiling
### Personal PC
```
g++ apfel_lhaout.cc -o apfel-lhaout-cc -I/home/anarendran/Documents/CEDAR_stuff/apfel/local/include -L/home/anarendran/Documents/CEDAR_stuff/apfel/local/lib -L/home/anarendran/Documents/CEDAR_stuff/LHAPDF-6.5.3/install/lib -lAPFEL -lLHAPDF
```

### PPE Cluster
```
g++ apfel_lhaout.cc -o apfel-lhaout-cc -I/home/ppe/a/anarendran/Documents/CEDAR_stuff/apfel/local/include -L/home/ppe/a/anarendran/Documents/CEDAR_stuff/apfel/local/lib -L/home/ppe/a/anarendran/Documents/CEDAR_stuff/LHAPDF-6.5.3/local/lib -lAPFEL -lLHAPDF
```

## Run with
```
PDF_APL=1.0 PDF_BPL=1.0 PDF_AHAD=1.1 PDF_BHAD=0.9 PDF_CHAD=1.0 PDF_AHADG=0.9 PDF_BHADG=1.1 PDF_CHADG=3.0 ./apfel-lhaout-cc
```
and variations thereof.

## Notes
With the number of X and Q points in lhaout as of 1/02/2023, it takes nearly a minute to run even one PDF. It's not broken :)


print(', '.join(str('{:.8e}'.format(x)) for x in a))
